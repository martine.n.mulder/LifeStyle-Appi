package dropwizard.model;

import com.fasterxml.jackson.annotation.JsonView;
import dropwizard.View;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import java.security.Principal;

/**
 * This is the model for the client entity, it contains
 * an empty constructor, a constructor for creating a
 * new client for the database, a constructor for saving
 * a client from the database and getters and setters.
 *
 * @author Martine Mulder
 */
public class Client implements Principal {

    @NotEmpty
    @Length(min = 1, max = 8)
    @JsonView(View.Public.class)
    private int id;

    @NotEmpty
    @Length(min = 3, max = 50)
    @JsonView(View.Public.class)
    private String firstName;

    @NotEmpty
    @Length(min = 1, max = 24)
    @JsonView(View.Public.class)
    private String preposition;

    @NotEmpty
    @Length(min = 3, max = 50)
    @JsonView(View.Public.class)
    private String lastName;

    @NotEmpty
    @Length(min = 1, max = 8)
    @JsonView(View.Private.class)
    private int coachId;

    @NotEmpty
    @Length(min = 1, max = 5)
    @JsonView(View.Public.class)
    private boolean active;

    @NotEmpty
    @Length(min = 1, max = 8)
    @JsonView(View.Public.class)
    private double weight;

    @NotEmpty
    @Length(min = 3, max = 10)
    @JsonView(View.Public.class)
    private String birthDate;

    @NotEmpty
    @Length(min = 1, max = 1)
    @JsonView(View.Public.class)
    private char gender;

    public Client() {

    }

    public Client(String firstName, String preposition, String lastName, int coachId, boolean active, double weight, String birthDate, char gender) {

        this.firstName = firstName;
        this.preposition = preposition;
        this.lastName = lastName;
        this.coachId = coachId;
        this.active = active;
        this.weight = weight;
        this.birthDate = birthDate;
        this.gender = gender;

    }

    public Client(int id, String firstName, String preposition, String lastName, int coachId, boolean active, double weight, String birthDate, char gender) {

        this.id = id;
        this.firstName = firstName;
        this.preposition = preposition;
        this.lastName = lastName;
        this.coachId = coachId;
        this.active = active;
        this.weight = weight;
        this.birthDate = birthDate;
        this.gender = gender;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPreposition() {
        return preposition;
    }

    public void setPreposition(String preposition) {
        this.preposition = preposition;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getCoachId() {
        return coachId;
    }

    public void setCoachId(int coachId) {
        this.coachId = coachId;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String getName() {
//        if (this.preposition.equals("")) {
//            return this.firstName + ' ' + this.lastName;
//        } else {
//            return this.firstName + ' ' + this.preposition + ' ' + this.lastName;
//        }
        return null;
    }

}