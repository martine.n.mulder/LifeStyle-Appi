package dropwizard.model;

import com.fasterxml.jackson.annotation.JsonView;
import dropwizard.View;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

public class Product {

    @NotEmpty
    @Length(min = 1, max = 11)
    @JsonView(View.Public.class)
    private int id;

    @NotEmpty
    @Length(min = 1, max = 100)
    @JsonView(View.Public.class)
    private String description;

    @Length(max = 50)
    @JsonView(View.Public.class)
    private String manufacturer;


    @JsonView(View.Public.class)
    private double amount;

    @Length(max = 25)
    @JsonView(View.Public.class)
    private String measureUnit;

    @Length(max = 1024)
    @JsonView(View.Public.class)
    private String commentaar;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String kcal;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String kjoule;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String stikstof;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String eiwit;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String eiwitPlant;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String eiwitDierlijk;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String vet;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String vetzurenTotaal;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String veturenVerzadigd;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String eov;
    private String mov;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String linolzuur;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String transvet;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String ala;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String epa;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String dha;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String cholesterol;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String koolydraten;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String suiker;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String mvKh;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String polyolen;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String vezels;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String water;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String alchol;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String calcium;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String fosfor;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String ijzerT;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String ijzerHaem;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String ijzerNonHaem;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String natrium;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String kalium;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String magnesium;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String zink;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String selenium;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String koper;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String jodium;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String retinol;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String bCarteen;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String vitB1;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String vitB2;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String vitB6;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String vitB12;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String vitD;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String vitE;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String vitC;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String folaat;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String foliumzuur;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String nicZuur;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String aTocoferol;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String bTocoferol;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String gammaTocoferol;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String dTocoferol;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String aCaroteen;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String luteine;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String zeaxanthine;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String bCryptoxanthine;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String lycopeen;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String vitK;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String vitK1;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String vitK2;

    @Length(max = 255)
    @JsonView(View.Public.class)
    private String cholecalciferol;

    @NotEmpty
    @JsonView(View.Public.class)
    private boolean addedByCoach;

    @NotEmpty
    @JsonView(View.Public.class)
    private boolean checked;

    @NotEmpty
    @Length(min = 1, max = 11)
    @JsonView(View.Public.class)
    private int userId;

    public Product(){

    }

    public Product(String description, String manufacturer, double amount, String measureUnit, String commentaar, String kcal, String kjoule, String stikstof, String eiwit, String eiwitPlant, String eiwitDierlijk, String vet, String vetzurenTotaal, String veturenVerzadigd, String eov, String mov, String linolzuur, String transvet, String ala, String epa, String dha, String cholesterol, String koolydraten, String suiker, String mvKh, String polyolen, String vezels, String water, String alchol, String calcium, String fosfor, String ijzerT, String ijzerHaem, String ijzerNonHaem, String natrium, String kalium, String magnesium, String zink, String selenium, String koper, String jodium, String retinol, String bCarteen, String vitB1, String vitB2, String vitB6, String vitB12, String vitD, String vitE, String vitC, String folaat, String foliumzuur, String nicZuur, String aTocoferol, String bTocoferol, String gammaTocoferol, String dTocoferol, String aCaroteen, String luteine, String zeaxanthine, String bCryptoxanthine, String lycopeen, String vitK, String vitK1, String vitK2, String cholecalciferol, boolean addedByCoach, boolean checked, int userId) {
        this.description = description;
        this.manufacturer = manufacturer;
        this.amount = amount;
        this.measureUnit = measureUnit;
        this.commentaar = commentaar;
        this.kcal = kcal;
        this.kjoule = kjoule;
        this.stikstof = stikstof;
        this.eiwit = eiwit;
        this.eiwitPlant = eiwitPlant;
        this.eiwitDierlijk = eiwitDierlijk;
        this.vet = vet;
        this.vetzurenTotaal = vetzurenTotaal;
        this.veturenVerzadigd = veturenVerzadigd;
        this.eov = eov;
        this.mov = mov;
        this.linolzuur = linolzuur;
        this.transvet = transvet;
        this.ala = ala;
        this.epa = epa;
        this.dha = dha;
        this.cholesterol = cholesterol;
        this.koolydraten = koolydraten;
        this.suiker = suiker;
        this.mvKh = mvKh;
        this.polyolen = polyolen;
        this.vezels = vezels;
        this.water = water;
        this.alchol = alchol;
        this.calcium = calcium;
        this.fosfor = fosfor;
        this.ijzerT = ijzerT;
        this.ijzerHaem = ijzerHaem;
        this.ijzerNonHaem = ijzerNonHaem;
        this.natrium = natrium;
        this.kalium = kalium;
        this.magnesium = magnesium;
        this.zink = zink;
        this.selenium = selenium;
        this.koper = koper;
        this.jodium = jodium;
        this.retinol = retinol;
        this.bCarteen = bCarteen;
        this.vitB1 = vitB1;
        this.vitB2 = vitB2;
        this.vitB6 = vitB6;
        this.vitB12 = vitB12;
        this.vitD = vitD;
        this.vitE = vitE;
        this.vitC = vitC;
        this.folaat = folaat;
        this.foliumzuur = foliumzuur;
        this.nicZuur = nicZuur;
        this.aTocoferol = aTocoferol;
        this.bTocoferol = bTocoferol;
        this.gammaTocoferol = gammaTocoferol;
        this.dTocoferol = dTocoferol;
        this.aCaroteen = aCaroteen;
        this.luteine = luteine;
        this.zeaxanthine = zeaxanthine;
        this.bCryptoxanthine = bCryptoxanthine;
        this.lycopeen = lycopeen;
        this.vitK = vitK;
        this.vitK1 = vitK1;
        this.vitK2 = vitK2;
        this.cholecalciferol = cholecalciferol;
        this.addedByCoach = addedByCoach;
        this.checked = checked;
        this.userId = userId;
    }

    public Product(int id, String description, String manufacturer, double amount, String measureUnit, String commentaar, String kcal, String kjoule, String stikstof, String eiwit, String eiwitPlant, String eiwitDierlijk, String vet, String vetzurenTotaal, String veturenVerzadigd, String eov, String mov, String linolzuur, String transvet, String ala, String epa, String dha, String cholesterol, String koolydraten, String suiker, String mvKh, String polyolen, String vezels, String water, String alchol, String calcium, String fosfor, String ijzerT, String ijzerHaem, String ijzerNonHaem, String natrium, String kalium, String magnesium, String zink, String selenium, String koper, String jodium, String retinol, String bCarteen, String vitB1, String vitB2, String vitB6, String vitB12, String vitD, String vitE, String vitC, String folaat, String foliumzuur, String nicZuur, String aTocoferol, String bTocoferol, String gammaTocoferol, String dTocoferol, String aCaroteen, String luteine, String zeaxanthine, String bCryptoxanthine, String lycopeen, String vitK, String vitK1, String vitK2, String cholecalciferol, boolean addedByCoach, boolean checked, int userId) {
        this.id = id;
        this.description = description;
        this.manufacturer = manufacturer;
        this.amount = amount;
        this.measureUnit = measureUnit;
        this.commentaar = commentaar;
        this.kcal = kcal;
        this.kjoule = kjoule;
        this.stikstof = stikstof;
        this.eiwit = eiwit;
        this.eiwitPlant = eiwitPlant;
        this.eiwitDierlijk = eiwitDierlijk;
        this.vet = vet;
        this.vetzurenTotaal = vetzurenTotaal;
        this.veturenVerzadigd = veturenVerzadigd;
        this.eov = eov;
        this.mov = mov;
        this.linolzuur = linolzuur;
        this.transvet = transvet;
        this.ala = ala;
        this.epa = epa;
        this.dha = dha;
        this.cholesterol = cholesterol;
        this.koolydraten = koolydraten;
        this.suiker = suiker;
        this.mvKh = mvKh;
        this.polyolen = polyolen;
        this.vezels = vezels;
        this.water = water;
        this.alchol = alchol;
        this.calcium = calcium;
        this.fosfor = fosfor;
        this.ijzerT = ijzerT;
        this.ijzerHaem = ijzerHaem;
        this.ijzerNonHaem = ijzerNonHaem;
        this.natrium = natrium;
        this.kalium = kalium;
        this.magnesium = magnesium;
        this.zink = zink;
        this.selenium = selenium;
        this.koper = koper;
        this.jodium = jodium;
        this.retinol = retinol;
        this.bCarteen = bCarteen;
        this.vitB1 = vitB1;
        this.vitB2 = vitB2;
        this.vitB6 = vitB6;
        this.vitB12 = vitB12;
        this.vitD = vitD;
        this.vitE = vitE;
        this.vitC = vitC;
        this.folaat = folaat;
        this.foliumzuur = foliumzuur;
        this.nicZuur = nicZuur;
        this.aTocoferol = aTocoferol;
        this.bTocoferol = bTocoferol;
        this.gammaTocoferol = gammaTocoferol;
        this.dTocoferol = dTocoferol;
        this.aCaroteen = aCaroteen;
        this.luteine = luteine;
        this.zeaxanthine = zeaxanthine;
        this.bCryptoxanthine = bCryptoxanthine;
        this.lycopeen = lycopeen;
        this.vitK = vitK;
        this.vitK1 = vitK1;
        this.vitK2 = vitK2;
        this.cholecalciferol = cholecalciferol;
        this.addedByCoach = addedByCoach;
        this.checked = checked;
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getMeasureUnit() {
        return measureUnit;
    }

    public void setMeasureUnit(String measureUnit) {
        this.measureUnit = measureUnit;
    }

    public String getCommentaar() {
        return commentaar;
    }

    public void setCommentaar(String commentaar) {
        this.commentaar = commentaar;
    }

    public String getKcal() {
        return kcal;
    }

    public void setKcal(String kcal) {
        this.kcal = kcal;
    }

    public String getKjoule() {
        return kjoule;
    }

    public void setKjoule(String kjoule) {
        this.kjoule = kjoule;
    }

    public String getStikstof() {
        return stikstof;
    }

    public void setStikstof(String stikstof) {
        this.stikstof = stikstof;
    }

    public String getEiwit() {
        return eiwit;
    }

    public void setEiwit(String eiwit) {
        this.eiwit = eiwit;
    }

    public String getEiwitPlant() {
        return eiwitPlant;
    }

    public void setEiwitPlant(String eiwitPlant) {
        this.eiwitPlant = eiwitPlant;
    }

    public String getEiwitDierlijk() {
        return eiwitDierlijk;
    }

    public void setEiwitDierlijk(String eiwitDierlijk) {
        this.eiwitDierlijk = eiwitDierlijk;
    }

    public String getVet() {
        return vet;
    }

    public void setVet(String vet) {
        this.vet = vet;
    }

    public String getVetzurenTotaal() {
        return vetzurenTotaal;
    }

    public void setVetzurenTotaal(String vetzurenTotaal) {
        this.vetzurenTotaal = vetzurenTotaal;
    }

    public String getVeturenVerzadigd() {
        return veturenVerzadigd;
    }

    public void setVeturenVerzadigd(String veturenVerzadigd) {
        this.veturenVerzadigd = veturenVerzadigd;
    }

    public String getEov() {
        return eov;
    }

    public void setEov(String eov) {
        this.eov = eov;
    }

    public String getMov() {
        return mov;
    }

    public void setMov(String mov) {
        this.mov = mov;
    }

    public String getLinolzuur() {
        return linolzuur;
    }

    public void setLinolzuur(String linolzuur) {
        this.linolzuur = linolzuur;
    }

    public String getTransvet() {
        return transvet;
    }

    public void setTransvet(String transvet) {
        this.transvet = transvet;
    }

    public String getAla() {
        return ala;
    }

    public void setAla(String ala) {
        this.ala = ala;
    }

    public String getEpa() {
        return epa;
    }

    public void setEpa(String epa) {
        this.epa = epa;
    }

    public String getDha() {
        return dha;
    }

    public void setDha(String dha) {
        this.dha = dha;
    }

    public String getCholesterol() {
        return cholesterol;
    }

    public void setCholesterol(String cholesterol) {
        this.cholesterol = cholesterol;
    }

    public String getKoolydraten() {
        return koolydraten;
    }

    public void setKoolydraten(String koolydraten) {
        this.koolydraten = koolydraten;
    }

    public String getSuiker() {
        return suiker;
    }

    public void setSuiker(String suiker) {
        this.suiker = suiker;
    }

    public String getMvKh() {
        return mvKh;
    }

    public void setMvKh(String mvKh) {
        this.mvKh = mvKh;
    }

    public String getPolyolen() {
        return polyolen;
    }

    public void setPolyolen(String polyolen) {
        this.polyolen = polyolen;
    }

    public String getVezels() {
        return vezels;
    }

    public void setVezels(String vezels) {
        this.vezels = vezels;
    }

    public String getWater() {
        return water;
    }

    public void setWater(String water) {
        this.water = water;
    }

    public String getAlchol() {
        return alchol;
    }

    public void setAlchol(String alchol) {
        this.alchol = alchol;
    }

    public String getCalcium() {
        return calcium;
    }

    public void setCalcium(String calcium) {
        this.calcium = calcium;
    }

    public String getFosfor() {
        return fosfor;
    }

    public void setFosfor(String fosfor) {
        this.fosfor = fosfor;
    }

    public String getIjzerT() {
        return ijzerT;
    }

    public void setIjzerT(String ijzerT) {
        this.ijzerT = ijzerT;
    }

    public String getIjzerHaem() {
        return ijzerHaem;
    }

    public void setIjzerHaem(String ijzerHaem) {
        this.ijzerHaem = ijzerHaem;
    }

    public String getIjzerNonHaem() {
        return ijzerNonHaem;
    }

    public void setIjzerNonHaem(String ijzerNonHaem) {
        this.ijzerNonHaem = ijzerNonHaem;
    }

    public String getNatrium() {
        return natrium;
    }

    public void setNatrium(String natrium) {
        this.natrium = natrium;
    }

    public String getKalium() {
        return kalium;
    }

    public void setKalium(String kalium) {
        this.kalium = kalium;
    }

    public String getMagnesium() {
        return magnesium;
    }

    public void setMagnesium(String magnesium) {
        this.magnesium = magnesium;
    }

    public String getZink() {
        return zink;
    }

    public void setZink(String zink) {
        this.zink = zink;
    }

    public String getSelenium() {
        return selenium;
    }

    public void setSelenium(String selenium) {
        this.selenium = selenium;
    }

    public String getKoper() {
        return koper;
    }

    public void setKoper(String koper) {
        this.koper = koper;
    }

    public String getJodium() {
        return jodium;
    }

    public void setJodium(String jodium) {
        this.jodium = jodium;
    }

    public String getRetinol() {
        return retinol;
    }

    public void setRetinol(String retinol) {
        this.retinol = retinol;
    }

    public String getbCarteen() {
        return bCarteen;
    }

    public void setbCarteen(String bCarteen) {
        this.bCarteen = bCarteen;
    }

    public String getVitB1() {
        return vitB1;
    }

    public void setVitB1(String vitB1) {
        this.vitB1 = vitB1;
    }

    public String getVitB2() {
        return vitB2;
    }

    public void setVitB2(String vitB2) {
        this.vitB2 = vitB2;
    }

    public String getVitB6() {
        return vitB6;
    }

    public void setVitB6(String vitB6) {
        this.vitB6 = vitB6;
    }

    public String getVitB12() {
        return vitB12;
    }

    public void setVitB12(String vitB12) {
        this.vitB12 = vitB12;
    }

    public String getVitD() {
        return vitD;
    }

    public void setVitD(String vitD) {
        this.vitD = vitD;
    }

    public String getVitE() {
        return vitE;
    }

    public void setVitE(String vitE) {
        this.vitE = vitE;
    }

    public String getVitC() {
        return vitC;
    }

    public void setVitC(String vitC) {
        this.vitC = vitC;
    }

    public String getFolaat() {
        return folaat;
    }

    public void setFolaat(String folaat) {
        this.folaat = folaat;
    }

    public String getFoliumzuur() {
        return foliumzuur;
    }

    public void setFoliumzuur(String foliumzuur) {
        this.foliumzuur = foliumzuur;
    }

    public String getNicZuur() {
        return nicZuur;
    }

    public void setNicZuur(String nicZuur) {
        this.nicZuur = nicZuur;
    }

    public String getaTocoferol() {
        return aTocoferol;
    }

    public void setaTocoferol(String aTocoferol) {
        this.aTocoferol = aTocoferol;
    }

    public String getbTocoferol() {
        return bTocoferol;
    }

    public void setbTocoferol(String bTocoferol) {
        this.bTocoferol = bTocoferol;
    }

    public String getGammaTocoferol() {
        return gammaTocoferol;
    }

    public void setGammaTocoferol(String gammaTocoferol) {
        this.gammaTocoferol = gammaTocoferol;
    }

    public String getdTocoferol() {
        return dTocoferol;
    }

    public void setdTocoferol(String dTocoferol) {
        this.dTocoferol = dTocoferol;
    }

    public String getaCaroteen() {
        return aCaroteen;
    }

    public void setaCaroteen(String aCaroteen) {
        this.aCaroteen = aCaroteen;
    }

    public String getLuteine() {
        return luteine;
    }

    public void setLuteine(String luteine) {
        this.luteine = luteine;
    }

    public String getZeaxanthine() {
        return zeaxanthine;
    }

    public void setZeaxanthine(String zeaxanthine) {
        this.zeaxanthine = zeaxanthine;
    }

    public String getbCryptoxanthine() {
        return bCryptoxanthine;
    }

    public void setbCryptoxanthine(String bCryptoxanthine) {
        this.bCryptoxanthine = bCryptoxanthine;
    }

    public String getLycopeen() {
        return lycopeen;
    }

    public void setLycopeen(String lycopeen) {
        this.lycopeen = lycopeen;
    }

    public String getVitK() {
        return vitK;
    }

    public void setVitK(String vitK) {
        this.vitK = vitK;
    }

    public String getVitK1() {
        return vitK1;
    }

    public void setVitK1(String vitK1) {
        this.vitK1 = vitK1;
    }

    public String getVitK2() {
        return vitK2;
    }

    public void setVitK2(String vitK2) {
        this.vitK2 = vitK2;
    }

    public String getCholecalciferol() {
        return cholecalciferol;
    }

    public void setCholecalciferol(String cholecalciferol) {
        this.cholecalciferol = cholecalciferol;
    }

    public boolean isAddedByCoach() {
        return addedByCoach;
    }

    public void setAddedByCoach(boolean addedByCoach) {
        this.addedByCoach = addedByCoach;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
