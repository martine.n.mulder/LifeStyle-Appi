package dropwizard.model;

import com.fasterxml.jackson.annotation.JsonView;
import dropwizard.View;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

public class DidYouKnow {
    @NotEmpty
    @Length(min = 1, max = 11)
    @JsonView(View.Public.class)
    private int id;

    @NotEmpty
    @Length(min = 1, max = 11)
    @JsonView(View.Public.class)
    private int userId;

    @NotEmpty
    @Length(min = 1, max = 254)
    @JsonView(View.Public.class)
    private String category;

    @NotEmpty
    @Length(min = 1, max = 254)
    @JsonView(View.Public.class)
    private String title;

    @NotEmpty
    @Length(min = 1, max = 2048)
    @JsonView(View.Public.class)
    private String body;

    @NotEmpty
    @Length(min = 1, max = 11)
    @JsonView(View.Public.class)
    private Boolean isChecked;

    public DidYouKnow() {
    }

    public DidYouKnow(int id, int userId, String category, String title, String body, Boolean isChecked) {
        this.id = id;
        this.userId = userId;
        this.category = category;
        this.title = title;
        this.body = body;
        this.isChecked = isChecked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Boolean getChecked() {
        return isChecked;
    }

    public void setChecked(Boolean checked) {
        isChecked = checked;
    }
}


