package dropwizard.model;

import com.fasterxml.jackson.annotation.JsonView;
import dropwizard.View;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import java.security.Principal;

public class Intake implements Principal {

    @NotEmpty
    @Length(min = 1, max = 8)
    @JsonView(View.Public.class)
    private int id;

    @NotEmpty
    @Length(min = 1, max = 8)
    @JsonView(View.Public.class)
    private int clientId;

    @NotEmpty
    @Length(min = 1, max = 8)
    @JsonView(View.Public.class)
    private String time;

    @NotEmpty
    @Length(min = 1, max = 8)
    @JsonView(View.Public.class)
    private int productId;

    @NotEmpty
    @Length(min = 1, max = 10)
    @JsonView(View.Public.class)
    private double amount;

    @NotEmpty
    @Length(min = 1, max = 8)
    @JsonView(View.Private.class)
    private int coachId;

    @NotEmpty
    @Length(min = 5, max = 10)
    @JsonView(View.Public.class)
    private String meal;

    @NotEmpty
    @Length(min = 3, max = 10)
    @JsonView(View.Public.class)
    private String date;

    @Override
    public String getName() {
        return null;
    }

    public Intake() {}

    public Intake(int id, int clientId, String time, int productId, double amount, String meal, String date) {

        this.id = id;
        this.clientId = clientId;
        this.time = time;
        this.productId = productId;
        this.amount = amount;
        this.meal = meal;
        this.date = date;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getCoachId() {
        return coachId;
    }

    public void setCoachId(int coachId) {
        this.coachId = coachId;
    }

    public String getMeal() {
        return meal;
    }

    public void setMeal(String meal) {
        this.meal = meal;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}