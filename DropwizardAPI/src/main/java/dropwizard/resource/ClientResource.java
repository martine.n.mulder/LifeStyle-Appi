package dropwizard.resource;

import com.fasterxml.jackson.annotation.JsonView;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import dropwizard.View;
import dropwizard.model.Client;
import dropwizard.model.User;
import dropwizard.service.ClientService;
import io.dropwizard.auth.Auth;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;

@Singleton
@Path("/clients")
@Produces(MediaType.APPLICATION_JSON)
public class ClientResource {

    private final ClientService service;

    @Inject
    public ClientResource(ClientService service) {
        this.service = service;
    }

    @GET
    @JsonView(View.Public.class)
    public Collection<Client> retrieveAll(@Auth User authenticator) {
        return service.getAll(authenticator);
    }

    @RolesAllowed("admin")
    @GET
    @Path("/usercount")
    @JsonView(View.Public.class)
    public int userCount(){
        return service.getUserCount();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @JsonView(View.Protected.class)
    public void createUser(@Auth User authenticator, Client client){
        service.add(authenticator, client);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @JsonView(View.Protected.class)
    public void updateClient(@Auth User authenticator, Client client) {
        service.update(authenticator, client);
    }

}