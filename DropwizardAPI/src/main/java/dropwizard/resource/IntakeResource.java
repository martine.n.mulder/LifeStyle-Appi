package dropwizard.resource;

import com.fasterxml.jackson.annotation.JsonView;
import dropwizard.View;
import dropwizard.model.Intake;
import dropwizard.service.IntakeService;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;

@Singleton
@Path("/intake")
@Produces(MediaType.APPLICATION_JSON)
public class IntakeResource {

    private final IntakeService service;

    @Inject
    public IntakeResource(IntakeService service) {
        this.service = service;
    }

    @GET
    @Path("{currentClientId}/{currentDate}")
    @JsonView(View.Public.class)
    public Collection<Intake> retrieveIntakePerDate(
            @PathParam("currentClientId") int currentClientId,
            @PathParam("currentDate") String currentDate) {

        return service.getIntakePerDate(currentClientId, currentDate);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @JsonView(View.Public.class)
    public void addIntake(Intake intake) {
        service.add(intake);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @JsonView(View.Public.class)
    public void updateIntake(Intake intake) {
        service.update(intake);
    }

    @DELETE
    @Path("/{id}")
    @JsonView(View.Public.class)
    public void delete( @PathParam("id") int id) {
        service.delete(id);
    }

}