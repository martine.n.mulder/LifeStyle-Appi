package dropwizard.resource;

import com.fasterxml.jackson.annotation.JsonView;
import dropwizard.View;
import dropwizard.model.Intake;
import dropwizard.model.Product;
import dropwizard.service.ProductService;


import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;
import java.util.Optional;

@Singleton
@Path("/products")
@Produces(MediaType.APPLICATION_JSON)
public class ProductResource {

    private final ProductService service;

    @Inject
    public ProductResource(ProductService service){this.service = service;}

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @JsonView(View.Public.class)
    public Collection<Product> retrieveAll(){
        return service.getAll();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @JsonView(View.Public.class)
    public void addProduct(Product product){
        service.add(product);
    }

    @RolesAllowed("admin")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{productId}")
    @JsonView(View.Public.class)
    public synchronized Product getProductById(@PathParam("productId") int productId) {
        return service.getProductById(productId);
    }

    @GET
    @Path("/addedByCoach")
    @Consumes(MediaType.APPLICATION_JSON)
    @JsonView(View.Public.class)
    public Collection<Product> getAddedProducts(){
        return service.getAddedProducts();
    }

    @PUT
    @Path("/edit")
    @Consumes(MediaType.APPLICATION_JSON)
    @JsonView(View.Public.class)
    public void editProduct(Product product){
        service.edit(product);
    }

    @DELETE
    @Path("/delete")
    @JsonView(View.Public.class)
    public void delete(@QueryParam("description") String description , @QueryParam("manufacturer") String manufacturer) {
        service.delete(description, manufacturer);
    }

    @GET
    @Path("/rivm")
    @Produces(MediaType.TEXT_PLAIN)
    public int countRivmProducts(){
        return service.getCountRivm();
    }

}
