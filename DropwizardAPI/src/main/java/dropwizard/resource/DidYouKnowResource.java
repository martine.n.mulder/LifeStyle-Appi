package dropwizard.resource;

import com.fasterxml.jackson.annotation.JsonView;
import dropwizard.View;
import dropwizard.model.DidYouKnow;
import dropwizard.service.DidYouKnowService;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.IOException;
import java.util.Collection;

@Singleton
@Path("/didYouKnow")
@Produces(MediaType.APPLICATION_JSON)
public class DidYouKnowResource {

    private final DidYouKnowService service;

    @Inject
    public DidYouKnowResource(DidYouKnowService service) {
        this.service = service;
    }

    @GET
    @JsonView(View.Public.class)
    public Collection<DidYouKnow> retrieveAll() {
        return service.getAll();
    }

    @POST
    @Path("/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public void upload(@DefaultValue("false") @FormDataParam("enabled") boolean disabled, @FormDataParam("file")File file,
                       @FormDataParam("file")FormDataContentDisposition fileDetail)throws IOException {
        service.upload(file, 1);
    }

    @POST
    @Path("/isChecked")
    @Consumes(MediaType.APPLICATION_JSON)
    public void isChecked(DidYouKnow didYouKnow){
        this.service.isChecked(didYouKnow);
    }

    @POST
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)
    public void update(DidYouKnow didYouKnow){
        this.service.updateDidYouKnow(didYouKnow);
    }

    @POST
    @Path("/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public void add(DidYouKnow didYouKnow){
        this.service.addDidYouKnow(didYouKnow);
    }

    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@QueryParam("id")int id){
        this.service.deleteDidYouKnow(id);
    }


}
