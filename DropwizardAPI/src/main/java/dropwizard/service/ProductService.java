package dropwizard.service;

import dropwizard.model.Product;
import dropwizard.persistence.ProductDAO;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Collection;

@Singleton
public class ProductService {
    private final ProductDAO dao;

    @Inject
    public ProductService(ProductDAO dao){
        this.dao = dao;
    }

    public Collection<Product> getAll(){
        return dao.allProducts();
    }

    public void add(Product product){
        dao.addProduct(product);
    }

    public void delete(String description, String manufacturer){
        dao.deleteProduct(description, manufacturer);
    }

    public void edit(Product product){
        dao.updateProduct(product);
    }

    public  Collection<Product> getAddedProducts(){return dao.getAddedProducts();}

    public Product getProductById(int productId) { return dao.getProductById(productId); }

    public int getCountRivm(){
        return dao.getCountRivm();
    }
}
