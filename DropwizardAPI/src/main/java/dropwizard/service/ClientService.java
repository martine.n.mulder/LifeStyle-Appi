package dropwizard.service;

import dropwizard.model.Client;
import dropwizard.model.User;
import dropwizard.persistence.ClientDAO;
import dropwizard.persistence.UserDAO;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.sql.SQLException;
import java.util.Collection;

@Singleton
public class ClientService extends BaseService {

    private final ClientDAO dao;

    @Inject
    public ClientService(ClientDAO dao) {
        this.dao = dao;
    }

    public Collection<Client> getAll(User authenticator) {

        return dao.getClients(authenticator);
    }

    public void add(User authenticator, Client client){
        dao.addClient(authenticator, client);
    }

    public void update(User authenticator, Client client) {
        dao.editClient(authenticator, client);
    }

    public int getUserCount() {
       return dao.getNumberOfClients();
    }

}