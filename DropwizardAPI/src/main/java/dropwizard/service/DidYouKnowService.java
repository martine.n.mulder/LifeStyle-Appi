package dropwizard.service;

import dropwizard.model.DidYouKnow;
import dropwizard.persistence.DidYouKnowDAO;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.*;
import java.util.Collection;

@Singleton
public class DidYouKnowService {
    private final DidYouKnowDAO dao;
    private final String splitRegex = ";";

    @Inject
    public DidYouKnowService(DidYouKnowDAO dao) {
        this.dao = dao;
    }

    public Collection<DidYouKnow> getAll(){
        return dao.getAll();
    }

    public void isChecked(DidYouKnow didYouKnow){
        didYouKnow.setChecked(true);
        dao.update(didYouKnow);
    }

    public void updateDidYouKnow(DidYouKnow didYouKnow){
        dao.update(didYouKnow);
    }

    public void addDidYouKnow(DidYouKnow didYouKnow){
        dao.insert(didYouKnow);
    }

    public void deleteDidYouKnow(int id){
        this.dao.delete(id);
    }

    public void upload(File file, int userId){
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            String line;
            while((line = bufferedReader.readLine()) != null){
                System.out.println(line);
                String data[] = line.split(splitRegex);
                DidYouKnow didYouKnow = new DidYouKnow();
                didYouKnow.setUserId(userId);
                didYouKnow.setTitle(data[0]);
                didYouKnow.setCategory(data[1]);
                didYouKnow.setBody(data[2]);
                didYouKnow.setChecked(false);
                dao.insert(didYouKnow);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
