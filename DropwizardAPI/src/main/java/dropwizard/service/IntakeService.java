package dropwizard.service;

import dropwizard.model.Intake;
import dropwizard.persistence.IntakeDAO;

import javax.inject.Inject;
import java.util.Collection;

public class IntakeService {

    private final IntakeDAO dao;

    @Inject
    public IntakeService(IntakeDAO dao) {
        this.dao = dao;
    }

    public Collection<Intake> getIntakePerDate(int currentClientId, String currentDate) {
        return dao.getIntakePerDate(currentClientId, currentDate);
    }

    public void add(Intake intake){
        dao.addIntake(intake);
    }

    public void update(Intake intake) {
        dao.editIntake(intake);
    }

    public void delete(int id){
        dao.deleteIntake(id);
    }

}
