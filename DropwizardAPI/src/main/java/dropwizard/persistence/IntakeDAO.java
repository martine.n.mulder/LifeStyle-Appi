package dropwizard.persistence;

import dropwizard.model.Intake;

import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * The IntakeDAO communicates with the intake
 * entity in the database. It contains methods
 * to add/edit intake and retrieve intake
 * from the database.
 *
 * @author Martine Mulder
 */
public class IntakeDAO {

    private PreparedStatement getClientIntakePerDate;
    private PreparedStatement addIntake;
    private  PreparedStatement editIntake;
    private PreparedStatement deleteIntake;
    private Connection dbConnection;
    private Database database;
    private ArrayList<Intake> theIntakePerDate;
    private ArrayList<Intake> breakfast;

    /**
     * The constructor for the IntakeDAO, it sets the database
     * connection, calls the prepared statements and the refresh
     * method to fill the necessary lists.
     */
    public IntakeDAO() {

        this.database = Database.getDatabase();
        this.dbConnection = database.getDbConnection();
        theIntakePerDate = new ArrayList<Intake>();
        breakfast = new ArrayList<Intake>();
        prepareStatements();

    }

    public Collection<Intake> getIntakePerDate(int currentClientId, String currentDate) {

		try {
		    theIntakePerDate.clear();
            getClientIntakePerDate.setInt(1, currentClientId);
            getClientIntakePerDate.setString(2, currentDate);

            ResultSet resultSet = getClientIntakePerDate.executeQuery();

            while (resultSet.next()) {
                Intake intake = new Intake(resultSet.getInt("inname_id"),
                        resultSet.getInt("inname_client_id"),
                        resultSet.getString("inname_tijd"),
                        resultSet.getInt("inname_product_id"),
                        resultSet.getDouble("inname_hoeveelheid"),
                        resultSet.getString("inname_maaltijd"),
                        resultSet.getString("inname_datum"));
                String[] time = intake.getTime().split(":");
                String newTime = time[0] + ":" + time[1];
                intake.setTime(newTime);
                theIntakePerDate.add(intake);
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return theIntakePerDate;
    }

    /**
     * This method sets the parameters for the PreparedStatement
     * addIntake and executes the query.
     *
     * @param intake the intake that will be added
     * @return true or false whether query successful
     */
    public Boolean addIntake(Intake intake) {
        try {
            addIntake.setInt(1, intake.getClientId());
            addIntake.setTime(2, java.sql.Time.valueOf(intake.getTime()));
            addIntake.setInt(3, intake.getProductId());
            addIntake.setDouble(4, intake.getAmount());
            addIntake.setString(5, intake.getMeal());
            addIntake.setDate(6, java.sql.Date.valueOf(intake.getDate()));
            addIntake.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * This method sets the parameters for the PreparedStatement
     * editIntake and executes the query.
     *
     * @param intake the intake that will be edited
     * @return true or false whether query successful
     */
    public Boolean editIntake(Intake intake) {
        try {
            editIntake.setTime(1, java.sql.Time.valueOf(intake.getTime()));
            editIntake.setInt(2, intake.getProductId());
            editIntake.setDouble(3, intake.getAmount());
            editIntake.setInt(4, intake.getId());
            editIntake.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * This method sets the parameters for the PreparedStatement
     * deleteIntake and executes the query.
     *
     * @param intakeId of the intake that will be deleted
     * @return true or false whether query successful
     */
    public Boolean deleteIntake(int intakeId) {
        try {
            deleteIntake.setInt(1,intakeId);
            deleteIntake.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * This method contains the prepared statements
     * necessary to communicate with the database entity.
     */
    private void prepareStatements() {
        try {
            getClientIntakePerDate = dbConnection.prepareStatement("SELECT * FROM inname WHERE inname_client_id = ? AND inname_datum = ? ORDER BY inname_tijd");
            addIntake = dbConnection.prepareStatement("INSERT INTO inname(inname_client_id, inname_tijd, inname_product_id, inname_hoeveelheid, inname_maaltijd, inname_datum) VALUES (?,?,?,?,?,?)");
            editIntake = dbConnection.prepareStatement("UPDATE inname SET inname_tijd = ?, inname_product_id = ?, inname_hoeveelheid = ? WHERE inname_id = ?");
            deleteIntake = dbConnection.prepareStatement("DELETE FROM inname WHERE inname_id = ?");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}