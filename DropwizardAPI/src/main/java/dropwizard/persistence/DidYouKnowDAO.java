package dropwizard.persistence;

import dropwizard.model.DidYouKnow;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

public class DidYouKnowDAO {
    private Connection dbConnection;
    private Database database = Database.getDatabase();

    private PreparedStatement insertDidyouKnow;
    private PreparedStatement selectAll;
    private PreparedStatement updateDidYouKnow;
    private PreparedStatement deleteDidYouKnow;

    public DidYouKnowDAO() {
        dbConnection = database.getDbConnection();
        preparedStatements();
    }

    public Collection<DidYouKnow> getAll(){
        Collection<DidYouKnow> allDidYouKnows = new  ArrayList<>();
        try {
            ResultSet resultSet = selectAll.executeQuery();
            while (resultSet.next()){
                allDidYouKnows.add(new DidYouKnow(
                        resultSet.getInt("wistjedat_id"),
                        resultSet.getInt("wistjedat_gebruiker_id"),
                        resultSet.getString("wistjedat_titel"),
                        resultSet.getString("wistjedat_string"),
                        resultSet.getString("wistjedat_categorie"),
                        resultSet.getBoolean("wistjedat_goedgekeurd")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return allDidYouKnows;
    }

    public void delete(int id){
        try {
            deleteDidYouKnow.setInt(1, id);
            deleteDidYouKnow.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insert(DidYouKnow didYouKnow){
        try {
            insertDidyouKnow.setInt(1, didYouKnow.getUserId());
            insertDidyouKnow.setString(2, didYouKnow.getTitle());
            insertDidyouKnow.setString(3, didYouKnow.getBody());
            insertDidyouKnow.setString(4, didYouKnow.getCategory());
            insertDidyouKnow.setBoolean(5, false);
            insertDidyouKnow.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void update(DidYouKnow didYouKnow){
        try {
            updateDidYouKnow.setInt(1, didYouKnow.getId());
            updateDidYouKnow.setString(2, didYouKnow.getTitle());
            updateDidYouKnow.setString(3, didYouKnow.getBody());
            updateDidYouKnow.setString(4,didYouKnow.getCategory());
            updateDidYouKnow.setBoolean(5, didYouKnow.getChecked());
            updateDidYouKnow.setInt(6, didYouKnow.getId());
            updateDidYouKnow.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void preparedStatements(){
        try {
            deleteDidYouKnow = dbConnection.prepareStatement("DELETE FROM wistjedat WHERE wistjedat_id = ?");
            insertDidyouKnow = dbConnection.prepareStatement("INSERT INTO wistjedat(wistjedat_gebruiker_id, wistjedat_titel, wistjedat_string, wistjedat_categorie, wistjedat_goedgekeurd) VALUES (?,?,?,?,?);");
            selectAll = dbConnection.prepareStatement("SELECT * FROM wistjedat");
            updateDidYouKnow = dbConnection.prepareStatement("UPDATE wistjedat SET wistjedat_gebruiker_id = ?, wistjedat_titel = ?, wistjedat_string = ?, wistjedat_categorie = ?, wistjedat_goedgekeurd = ? WHERE wistjedat_id = ?;");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
