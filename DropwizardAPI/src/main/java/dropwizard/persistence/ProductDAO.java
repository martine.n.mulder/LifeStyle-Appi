package dropwizard.persistence;

import dropwizard.model.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ProductDAO {

    Database database = Database.getDatabase();
    private PreparedStatement getAllProducts;
    private PreparedStatement addProduct;
    private PreparedStatement updateProduct;
    private PreparedStatement deleteProduct;
    private PreparedStatement getAddedProducts;
    private PreparedStatement deleteNullProduct;
    private PreparedStatement getProductById;
    private PreparedStatement countRivm;
    private Connection dbConnection;
    private final List<Product> addedProducts;
    private final List<Product> allProducts;
    private final List<Product> allRivmProducts;

    public ProductDAO() {
        dbConnection = database.getDbConnection();
        preparedStatements();
        allProducts = new ArrayList<>();
        addedProducts = new ArrayList<>();
        allRivmProducts = new ArrayList<>();
    }

    public Collection<Product> allProducts(){
        ResultSet resultSet;
        try {
            resultSet = getAllProducts.executeQuery();
            return createProduct(resultSet, allProducts);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection<Product> getAddedProducts(){
        ResultSet resultSet;
        try {
            resultSet = getAddedProducts.executeQuery();
            return createProduct((resultSet), addedProducts) ;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * This method returns a Product object that matches
     * the given id by calling the function selectSpecificProduct.
     *
     * @param productId requested product id
     * @return a specific Product
     */
    public synchronized Product getProductById(int productId) {
        List<Product> products = new ArrayList<>();
        try {
            getProductById.setInt(1, productId);
            ResultSet resultSet = getProductById.executeQuery();
            products = createProduct(resultSet, products);
            if (products != null && products.size() > 0) {
                System.out.println("Product " + products.get(0));
                return products.get(0);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new Product();

    }

    public int getCountRivm() {
        try {
            ResultSet resultSet = countRivm.executeQuery();
            if (resultSet.next()){
                return resultSet.getInt("aantal");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void addProduct(Product product){
        try {
            setPreparedStatement(product, addProduct);
            addProduct.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

        public void updateProduct(Product product){
            try {
                setPreparedStatement(product, updateProduct);
                updateProduct.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

    public void deleteProduct(String description, String manufacturer){
        try {
            PreparedStatement usedStatement = deleteProduct;

            if(manufacturer == null){
                usedStatement = deleteNullProduct;
            }
            usedStatement.setObject(1, description);
            usedStatement.setObject(2, manufacturer);
            System.out.println(usedStatement);
            usedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private PreparedStatement setPreparedStatement(Product product, PreparedStatement statement){
        try {
            statement.setString( 1, product.getDescription());
            statement.setString(2,product.getManufacturer());
            statement.setDouble(3,product.getAmount());
            statement.setString( 4, product.getMeasureUnit());
            statement.setString(5,product.getCommentaar());
            statement.setString(6,product.getKcal());
            statement.setString(7,product.getKjoule());
            statement.setString(8,product.getStikstof());
            statement.setString(9,product.getEiwit());
            statement.setString(10, product.getEiwitPlant());
            statement.setString(11,product.getEiwitDierlijk());
            statement.setString(12,product.getVet());
            statement.setString(13,product.getVetzurenTotaal());
            statement.setString(14,product.getVeturenVerzadigd());
            statement.setString(15,product.getEov());
            statement.setString(16,product.getMov());
            statement.setString(17,product.getLinolzuur());
            statement.setString(18,product.getTransvet());
            statement.setString(19,product.getAla());
            statement.setString(20,product.getEpa());
            statement.setString(21,product.getDha());
            statement.setString(22,product.getCholesterol());
            statement.setString(23,product.getKoolydraten());
            statement.setString(24,product.getSuiker());
            statement.setString(25,product.getMvKh());
            statement.setString(26,product.getPolyolen());
            statement.setString(27,product.getVezels());
            statement.setString(28,product.getWater());
            statement.setString(29,product.getAlchol());
            statement.setString(30,product.getCalcium());
            statement.setString(31,product.getFosfor());
            statement.setString(32,product.getIjzerT());
            statement.setString(33,product.getIjzerHaem());
            statement.setString(34,product.getIjzerNonHaem());
            statement.setString(35,product.getNatrium());
            statement.setString(36,product.getKalium());
            statement.setString(37,product.getMagnesium());
            statement.setString(38,product.getZink());
            statement.setString(39,product.getSelenium());
            statement.setString(40,product.getKoper());
            statement.setString(41,product.getJodium());
            statement.setString(42,product.getRetinol());
            statement.setString(43,product.getbCarteen());
            statement.setString(44,product.getVitB1());
            statement.setString(45,product.getVitB2());
            statement.setString(46,product.getVitB6());
            statement.setString(47,product.getVitB12());
            statement.setString(48,product.getVitD());
            statement.setString(49,product.getVitE());
            statement.setString(50,product.getVitC());
            statement.setString(51,product.getFolaat());
            statement.setString(52,product.getFoliumzuur());
            statement.setString(53,product.getNicZuur());
            statement.setString(54,product.getaTocoferol());
            statement.setString(55,product.getbTocoferol());
            statement.setString(56,product.getGammaTocoferol());
            statement.setString(57,product.getdTocoferol());
            statement.setString(58,product.getaCaroteen());
            statement.setString(59,product.getLuteine());
            statement.setString(60,product.getZeaxanthine());
            statement.setString(61,product.getbCryptoxanthine());
            statement.setString(62,product.getLycopeen());
            statement.setString(63,product.getVitK());
            statement.setString(64,product.getVitK1());
            statement.setString(65,product.getVitK2());
            statement.setString(66,product.getCholecalciferol());
            statement.setBoolean(67,product.isAddedByCoach());
            statement.setBoolean(68,product.isChecked());
            statement.setInt(69,product.getUserId());
            if(product.getId() != 0){
                statement.setInt(70, product.getId());
            }
            System.out.println("statement = " + statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return statement;
    }

    private List<Product> createProduct(ResultSet resultSet, List<Product> products){
        try {
            products.clear();
            while(resultSet.next()) {
                 products.add(new Product(
                        resultSet.getInt("product_id"),
                        resultSet.getString("product_omschrijving"),
                        resultSet.getString("product_fabrikant"),
                        resultSet.getDouble("product_hoeveelheid"),
                        resultSet.getString("product_eenheid"),
                        resultSet.getString("product_commentaar"),
                        resultSet.getString("product_kcal"),
                        resultSet.getString("product_kjoule"),
                        resultSet.getString("product_stikstof"),
                        resultSet.getString("product_eiwit"),
                        resultSet.getString("product_eiwit_plant"),
                        resultSet.getString("product_eiwit_dierlijk"),
                        resultSet.getString("product_vet"),
                        resultSet.getString("product_vetzuren_totaal"),
                        resultSet.getString("product_vetzuren_verzadigd"),
                        resultSet.getString("product_eov"),
                        resultSet.getString("product_mov"),
                        resultSet.getString("product_linolzuur"),
                        resultSet.getString("product_transvet"),
                        resultSet.getString("product_ala"),
                        resultSet.getString("product_epa"),
                        resultSet.getString("product_dha"),
                        resultSet.getString("product_cholesterol"),
                        resultSet.getString("product_koolydraten"),
                        resultSet.getString("product_suiker"),
                        resultSet.getString("product_mv_kh"),
                        resultSet.getString("product_polyolen"),
                        resultSet.getString("product_vezels"),
                        resultSet.getString("product_water"),
                        resultSet.getString("product_alchol"),
                        resultSet.getString("product_calcium"),
                        resultSet.getString("product_fosfor"),
                        resultSet.getString("product_ijzer_t"),
                        resultSet.getString("product_ijzer_haem"),
                        resultSet.getString("product_ijzer_non_haem"),
                        resultSet.getString("product_natrium"),
                        resultSet.getString("product_kalium"),
                        resultSet.getString("product_magnesium"),
                        resultSet.getString("product_zink"),
                        resultSet.getString("product_selenium"),
                        resultSet.getString("product_koper"),
                        resultSet.getString("product_jodium"),
                        resultSet.getString("product_retinol"),
                        resultSet.getString("product_b_carteen"),
                        resultSet.getString("product_vit_b1"),
                        resultSet.getString("product_vit_b2"),
                        resultSet.getString("product_vit_b6"),
                        resultSet.getString("product_vit_b12"),
                        resultSet.getString("product_vit_d"),
                        resultSet.getString("product_vit_e"),
                        resultSet.getString("product_vit_c"),
                        resultSet.getString("product_folaat"),
                        resultSet.getString("product_foliumzuur"),
                        resultSet.getString("product_nic_zuur"),
                        resultSet.getString("product_a_tocoferol"),
                        resultSet.getString("product_b_tocoferol"),
                        resultSet.getString("product_gamma_tocoferol"),
                        resultSet.getString("product_d_tocoferol"),
                        resultSet.getString("product_a_caroteen"),
                        resultSet.getString("product_luteine"),
                        resultSet.getString("product_zeaxanthine"),
                        resultSet.getString("product_b_cryptoxanthine"),
                        resultSet.getString("product_lycopeen"),
                        resultSet.getString("product_vit_k"),
                        resultSet.getString("product_vit_k1"),
                        resultSet.getString("product_vit_k2"),
                        resultSet.getString("product_cholecalciferol"),
                        resultSet.getBoolean("product_zelf_toegevoegd"),
                        resultSet.getBoolean("product_gecheckt"),
                        resultSet.getInt("product_gebruiker_id")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return products;
    }

    private void preparedStatements(){
        try {
            countRivm = dbConnection.prepareStatement("SELECT COUNT(*) AS aantal FROM product");
            getProductById = dbConnection.prepareStatement("SELECT * from product WHERE product_id = ?;");
            getAllProducts = dbConnection.prepareStatement("SELECT * from product;");
            addProduct = dbConnection.prepareStatement("INSERT INTO product(product_omschrijving,product_fabrikant,product_hoeveelheid,product_eenheid,product_commentaar,product_kcal,product_kjoule,product_stikstof,product_eiwit,product_eiwit_plant,product_eiwit_dierlijk,product_vet,product_vetzuren_totaal,product_vetzuren_verzadigd,product_eov,product_mov,product_linolzuur,product_transvet,product_ala,product_epa,product_dha,product_cholesterol,product_koolydraten,product_suiker,product_mv_kh,product_polyolen,product_vezels,product_water,product_alchol,product_calcium,product_fosfor,product_ijzer_t,product_ijzer_haem,product_ijzer_non_haem,product_natrium,product_kalium,product_magnesium,product_zink,product_selenium,product_koper,product_jodium,product_retinol,product_b_carteen,product_vit_b1,product_vit_b2,product_vit_b6,product_vit_b12,product_vit_d,product_vit_e,product_vit_c,product_folaat,product_foliumzuur,product_nic_zuur,product_a_tocoferol,product_b_tocoferol,product_gamma_tocoferol,product_d_tocoferol,product_a_caroteen,product_luteine,product_zeaxanthine,product_b_cryptoxanthine,product_lycopeen,product_vit_k,product_vit_k1,product_vit_k2,product_cholecalciferol,product_zelf_toegevoegd,product_gecheckt,product_gebruiker_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
            updateProduct = dbConnection.prepareStatement("UPDATE product SET product_omschrijving = ?, product_fabrikant = ?, product_hoeveelheid = ?, product_eenheid = ?, product_commentaar = ?, product_kcal = ?, product_kjoule = ?, product_stikstof = ?, product_eiwit = ?, product_eiwit_plant = ?, product_eiwit_dierlijk = ?, product_vet = ?, product_vetzuren_totaal = ?, product_vetzuren_verzadigd = ?, product_eov = ?, product_mov = ?, product_linolzuur = ?, product_transvet = ?, product_ala = ?, product_epa = ?, product_dha = ?, product_cholesterol = ?, product_koolydraten = ?, product_suiker = ?, product_mv_kh = ?, product_polyolen = ?, product_vezels = ?, product_water = ?, product_alchol = ?, product_calcium = ?, product_fosfor = ?, product_ijzer_t = ?, product_ijzer_haem = ?, product_ijzer_non_haem = ?, product_natrium = ?, product_kalium = ?, product_magnesium = ?, product_zink = ?, product_selenium = ?, product_koper = ?, product_jodium = ?, product_retinol = ?, product_b_carteen = ?, product_vit_b1 = ?, product_vit_b2 = ?, product_vit_b6 = ?, product_vit_b12 = ?, product_vit_d = ?, product_vit_e = ?, product_vit_c = ?, product_folaat = ?, product_foliumzuur = ?, product_nic_zuur = ?, product_a_tocoferol = ?, product_b_tocoferol = ?, product_gamma_tocoferol = ?, product_d_tocoferol = ?, product_a_caroteen = ?, product_luteine = ?, product_zeaxanthine = ?, product_b_cryptoxanthine = ?, product_lycopeen = ?, product_vit_k = ?, product_vit_k1 = ?, product_vit_k2 = ?, product_cholecalciferol = ?, product_zelf_toegevoegd = ?, product_gecheckt = ?, product_gebruiker_id = ? WHERE product_id = ? ;");
            deleteProduct = dbConnection.prepareStatement("DELETE FROM product WHERE product_omschrijving = ? AND product_fabrikant = ?;");
            getAddedProducts = dbConnection.prepareStatement("SELECT * from product WHERE product_zelf_toegevoegd = TRUE;");
            deleteNullProduct = dbConnection.prepareStatement("DELETE FROM product WHERE product_omschrijving = ? AND product_fabrikant IS ?;");
           } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
