package dropwizard.persistence;

import dropwizard.model.Client;
import dropwizard.model.Product;
import dropwizard.model.User;
import io.dropwizard.auth.Auth;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

/**
 * The Client DAO communicates with the client
 * entity in the database. It contains methods
 * to add/edit a client and retrieve the clients
 * from the database.
 *
 * @author Martine Mulder
 */
public class ClientDAO {

    private PreparedStatement allClients;
    private PreparedStatement addClient;
    private PreparedStatement editClient;
    private PreparedStatement totalClients;
    private Connection dbConnection;
    private Database database;
    private ArrayList<Client> theClients;
    private ResultSet resultSet;
    private int userCount;
    private UserDAO userDAO;

    /**
     * The constructor for the ClientDAO, it sets the database
     * connection, calls the prepared statements and the refresh
     * method to fill the necessary lists.
     *
     */
    public ClientDAO(UserDAO userDAO) {

        this.database = Database.getDatabase();
        this.dbConnection = database.getDbConnection();
        this.userDAO = userDAO;

        prepareStatements();

    }

    public int getNumberOfClients() {
        try {
            resultSet = totalClients.executeQuery();
            userCount = resultSet.getInt(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userCount;
    }

    /**
     * @return a list with the clients of the logged in user
     */
    public Collection<Client> getClients(User authenticator) {

        User user = userDAO.getByUsername(authenticator.getUsername());

        try {
            theClients = new ArrayList<Client>();
            allClients.setInt(1,user.getId());
            ResultSet resultSet = allClients.executeQuery();

            while (resultSet.next()) {
                Client client = new Client(
                    resultSet.getInt("client_id"),
                    resultSet.getString("client_voornaam"),
                    resultSet.getString("client_tussenvoegsel"),
                    resultSet.getString("client_achternaam"),
                    resultSet.getInt("client_gebruiker_id"),
                    resultSet.getBoolean("client_actief"),
                    resultSet.getDouble("client_gewicht"),
                    resultSet.getString("client_geboortedatum"),
                    resultSet.getString("client_geslacht").charAt(0)
                );
                theClients.add(client);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
        return theClients;

    }

    /**
     * This method sets the parameters for the PreparedStatement
     * addClient and executes the query.
     *
     * @param client the client object that will be added
     * @return true or false whether query successful
     */
    public Boolean addClient(User authenticator, Client client) {
        client.setCoachId(userDAO.getByUsername(authenticator.getUsername()).getId());

        try {
            addClient.setString(1, client.getFirstName());
            addClient.setString(2, client.getPreposition());
            addClient.setString(3, client.getLastName());
            addClient.setInt(4, client.getCoachId());
            addClient.setBoolean(5, client.isActive());
            addClient.setDouble(6, client.getWeight());
            addClient.setDate(7, java.sql.Date.valueOf(client.getBirthDate()));
            addClient.setString(8, String.valueOf(client.getGender()));
            addClient.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * This method sets the parameters for the PreparedStatement
     * editClient and executes the query.
     *
     * @param client the client object that will be edited
     * @return true or false whether query successful
     */
    public Boolean editClient(User authenticator, Client client) {
        client.setCoachId(userDAO.getByUsername(authenticator.getUsername()).getId());
        try {
            editClient.setString(1, client.getFirstName());
            editClient.setString(2, client.getPreposition());
            editClient.setString(3, client.getLastName());
            editClient.setInt(4, client.getCoachId());
            editClient.setBoolean(5, client.isActive());
            editClient.setDouble(6, client.getWeight());
            editClient.setDate(7, java.sql.Date.valueOf(client.getBirthDate()));
            editClient.setString(8, String.valueOf(client.getGender()));
            editClient.setInt(9, client.getId());
            editClient.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * This method contains the prepared statements
     * necessary to communicate with the database entity.
     */
    private void prepareStatements() {
        try {
            allClients = dbConnection.prepareStatement("SELECT * FROM client WHERE client_actief = 1 AND client_gebruiker_id = ?");
            totalClients = dbConnection.prepareStatement("SELECT COUNT(*) FROM client WHERE client_actief = 1");
            addClient = dbConnection.prepareStatement("INSERT INTO client(client_voornaam, client_tussenvoegsel, client_achternaam, client_gebruiker_id, client_actief, client_gewicht, client_geboortedatum, client_geslacht) VALUES (?,?,?,?,?,?,?,?)");
            editClient = dbConnection.prepareStatement("UPDATE client SET client_voornaam = ?, client_tussenvoegsel = ?, client_achternaam = ?, client_gebruiker_id = ?, client_actief = ?, client_gewicht = ?, client_geboortedatum = ?, client_geslacht = ? WHERE client_id = ?");
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}