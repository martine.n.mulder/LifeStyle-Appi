import { Component, OnInit } from '@angular/core';
import {ProductService} from "./product.service"
import {Product} from "./Product"
import {Observable} from "rxjs/Observable";
import {LoginComponent} from "../user/login/login.component";


@Component({
    selector: 'app-products',
    templateUrl: './products.component.html',
    styleUrls: ['./products.component.css'],
    providers: [ProductService, LoginComponent, ProductsComponent]

})

export class ProductsComponent implements OnInit {

    public addedProducts: Observable<Product[]>;
    selectedRow : number;
    selectedProduct: Product = new Product();
    oldDescription: string;


    settings = {

        //actions: false,
        //mode: 'external',
        //add: {confirmCreate: true},
        pager : {
            display: true,
            perPage: 20
        },
        columns: {

            description: {
                title: 'Omschrijving'
            },
            manufacturer: {
                title: 'Fabrikant'
            },
            amount: {
                title: 'Hoeveelheid'
            },
            measureUnit: {
                title: 'Eenheid'
            }, commentaar: {
                title: 'Commentaar'
            },
            kcal: {
                title: 'Kcal'
            },
            kjoule: {
                title: 'Kjoule'
            },
            stikstof: {
                title: 'Stikstof'
            },
            eiwit: {
                title: 'Eiwit'
            },
            eiwit_plant: {
                title: 'Eiwit-plant'
            },
            eiwit_dierlijk: {
                title: 'Eiwit-dierlijk'
            },
            vet: {
                title: 'Vet'
            }, vetzuren_totaal: {
                title: 'Vetzuren-totaal'
            },
            vetzuren_verzadigd: {
                title: 'Vetzuren-verzadigd'
            },
            eov: {
                title: 'Eov'
            },
            mov: {
                title: 'Mov'
            },
            linolzuur: {
                title: 'Eiwit'
            },
            transvet: {
                title: 'Transvet'
            },
            ala: {
                title: 'Ala-dierlijk'
            },
            epa: {
                title: 'Epa'
            }, dha: {
                title: 'Dha'
            },
            cholesterol: {
                title: 'Cholesterol'
            },
            koolydraten: {
                title: 'Koolhydraten'
            },
            suiker: {
                title: 'Suiker'
            },
            mv_kh: {
                title: 'Mv_kh'
            },
            polyolen: {
                title: 'Polyolen'
            },
            vezels: {
                title: 'Vezels'
            },
            water: {
                title: 'Water'
            }, alchol: {
                title: 'Alcohol_totaal'
            },
            calcium: {
                title: 'Calcium'
            },
            fosfor: {
                title: 'Fosfor'
            },
            ijzer_t: {
                title: 'Ijzer_t'
            },
            ijzer_haem: {
                title: 'Ijzer_haem'
            },
            ijzer_non_haem: {
                title: 'Ijzer_non_haem'
            }, natrium: {
                title: 'Natrium'
            },
            kalium: {
                title: 'Kalium'
            },
            magnesium: {
                title: 'Magnesium'
            },
            zink: {
                title: 'Zink'
            },
            selenium: {
                title: 'Selenium'
            },
            koper: {
                title: 'Koper'
            }, jodium: {
                title: 'Jodium'
            },
            retinol: {
                title: 'Retinol'
            },
            b_carteen: {
                title: 'B-Carteen'
            },
            vit_b1: {
                title: 'Vit-B1'
            },
            vit_b2: {
                title: 'Vit-B2'
            },
            vit_b6: {
                title: 'Vit-B6'
            }, vit_b12: {
                title: 'Vit-B12'
            },
            vit_d: {
                title: 'Vit-D'
            },
            vit_e: {
                title: 'Vit-E'
            },
            vit_c: {
                title: 'Vit-C'
            },
            folaat: {
                title: 'Folaat'
            },
            foliumzuur: {
                title: 'Foliumzuur'
            }, nic_zuur: {
                title: 'Nic-Zuur'
            },
            a_tocoferol: {
                title: 'A-Tocoferol'
            },
            b_tocoferol: {
                title: 'B-Tocoferol'
            },
            gamma_tocoferol: {
                title: 'Gamma-Tocoferol'
            },
            d_tocoferol: {
                title: 'D-Tocoferol'
            }, a_caroteen: {
                title: 'A-Caroteen'
            },
            luteine: {
                title: 'Luteine'
            },
            zeaxanthine: {
                title: 'Zeaxanthine'
            },
            lycopeen: {
                title: 'Lycopeen'
            },
            vit_k: {
                title: 'Vit-K'
            }, vit_k1: {
                title: 'Vit-K1'
            },
            vit_k2: {
                title: 'Vit-K2'
            },
            cholecalciferol: {
                title: 'Cholecalciferol'
            },
        }
    };

    constructor(private productService: ProductService) {
        this.getAddedProducts()
    }

    onSelect(event): void {
        this.selectedProduct = event.data;
        this.oldDescription = event.data.description;
    }

    productToevoegen(event) {
    //this.productService.addProduct(this.selectedProduct);
        console.log(event)

    }

    editProduct(){
        this.productService.editProduct(this.selectedProduct);

    }

    productVerwijderen() {
        console.log("Verwijderen");
        this.productService.deleteProduct(this.selectedProduct)
    }

    console() {
            console.log(this.selectedProduct.description)
    }

    ngOnInit() {
        this.getAddedProducts();

    }

    getAddedProducts()    {
        this.addedProducts = this.productService.getAddedProducts();
        }

    public hasData()
    {
        return this.addedProducts !== null;
    }
}
