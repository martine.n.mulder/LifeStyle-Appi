import { Injectable } from '@angular/core';
import {Product } from "./product";
import { AuthorizationService } from '../shared/authorization.service';
import { LoginComponent } from "../user/login/login.component";
import { Subject } from "rxjs/Subject";
import {ApiService} from "../shared/api.service";
import {Observable} from "rxjs/Observable";


@Injectable()
export class ProductService {

    productArray: Product[];

    constructor(private api: ApiService, private authService: AuthorizationService, private loginComponent: LoginComponent) {
        this.createProductArray();
    }

    public getAllProducts(): Observable<Product[]> {
        return this.api.get<Product[]>('products')
    }

    public getRivmProducts(): Observable<Product[]> {
        return this.api.get<Product[]>('products/rivm')
    }

    public getRivmCount() {
        let text;
        text = this.api.get('products/rivm')
                 .map(response =>
                     response.toString());
        return text;
    }

    public createProductArray() {
        let tempList
        this.api.get<Product[]>('products').subscribe(
            result =>{tempList = result},
            error =>{console.log(error)},
            ()=>{this.productArray = tempList});

    }

    public getProductById(productId : number): Observable<any> {
        // console.log('Getting specific product');
        return this.api.get<Product>('products/' +productId);
        //     .map((product: Array<any>) => {
        //         let result:Array<Product> = [];
        //         if (product) {
        //             product.forEach((erg) => {
        //                 result.push(new Product(erg.id, erg.description()));
        //             });
        //         }
        //         return result; // <<<=== missing return
        //     })
        //     .subscribe(product => this.product = product);
    }

    public addProduct(product: Product): void {
        let data =
            {
                description: product.description,
                manufacturer: product.manufacturer,
                amount: product.amount,
                measureUnit: product.measureUnit,
                commentaar: product.commentaar,
                kcal: product.kcal,
                kjoule: product.kjoule,
                stikstof: product.stikstof,
                eiwit: product.eiwit,
                eiwitPlant: product.eiwitPlant,
                eiwitDierlijk: product.eiwitDierlijk,
                vet: product.vet,
                vetzurenTotaal: product.vetzurenTotaal,
                veturenVerzadigd: product.veturenVerzadigd,
                eov: product.eov,
                mov: product.mov,
                linolzuur: product.linolzuur,
                transvet: product.transvet,
                ala: product.ala,
                epa: product.epa,
                dha: product.dha,
                cholesterol: product.cholesterol,
                koolydraten: product.koolydraten,
                suiker: product.suiker,
                mvKh: product.mvKh,
                polyolen: product.polyolen,
                vezels: product.vezels,
                water: product.water,
                alchol: product.alchol,
                calcium: product.calcium,
                fosfor: product.fosfor,
                ijzerT: product.ijzerT,
                ijzerHaem: product.ijzerHaem,
                ijzerNonHaem: product.ijzerNonHaem,
                natrium: product.natrium,
                kalium: product.kalium,
                magnesium: product.magnesium,
                zink: product.zink,
                selenium: product.selenium,
                koper: product.koper,
                jodium: product.jodium,
                retinol: product.retinol,
                bCarteen: product.bCarteen,
                vitB1: product.vitB1,
                vitB2: product.vitB2,
                vitB6: product.vitB6,
                vitB12: product.vitB12,
                vitD: product.vitD,
                vitE: product.vitE,
                vitC: product.vitC,
                folaat: product.folaat,
                foliumzuur: product.foliumzuur,
                nicZuur: product.nicZuur,
                aTocoferol: product.aTocoferol,
                bTocoferol: product.bTocoferol,
                gammaTocoferol: product.gammaTocoferol,
                dTocoferol: product.dTocoferol,
                aCaroteen: product.aCaroteen,
                luteine: product.luteine,
                zeaxanthine: product.zeaxanthine,
                bCryptoxanthine: product.bCryptoxanthine,
                lycopeen: product.lycopeen,
                vitK: product.vitK,
                vitK1: product.vitK1,
                vitK2: product.vitK2,
                cholecalciferol: product.cholecalciferol,
                addedByCoach: true,
                checked: false,
                userId: 1
            };

            this.api.post('/products', data).subscribe(

                data => {
                    alert('Het toevoegen is gelukt');
                },
                error =>{
                    alert('Het toevoegen is niet gelukt');
                }
            )
    }

    public getAddedProducts(): Observable<Product[]> {
        return this.api.get<Product[]>('products/addedByCoach')
    }

    public deleteProduct(product:Product) {
        console.log(product.description)
        this.api.delete<void>('products/delete', {description: product.description, manufacturer: product.manufacturer})
            .subscribe(
                (response) => alert('Product successfully deleted.'),
                (error) => alert('Product is niet verwijderd')
            );
    }

    public editProduct(product: Product): void {
        let data = {
                id: product.id,
                description: product.description,
                manufacturer: product.manufacturer,
                amount: product.amount,
                measureUnit: product.measureUnit,
                commentaar: product.commentaar,
                kcal: product.kcal,
                kjoule: product.kjoule,
                stikstof: product.stikstof,
                eiwit: product.eiwit,
                eiwitPlant: product.eiwitPlant,
                eiwitDierlijk: product.eiwitDierlijk,
                vet: product.vet,
                vetzurenTotaal: product.vetzurenTotaal,
                veturenVerzadigd: product.veturenVerzadigd,
                eov: product.eov,
                mov: product.mov,
                linolzuur: product.linolzuur,
                transvet: product.transvet,
                ala: product.ala,
                epa: product.epa,
                dha: product.dha,
                cholesterol: product.cholesterol,
                koolydraten: product.koolydraten,
                suiker: product.suiker,
                mvKh: product.mvKh,
                polyolen: product.polyolen,
                vezels: product.vezels,
                water: product.water,
                alchol: product.alchol,
                calcium: product.calcium,
                fosfor: product.fosfor,
                ijzerT: product.ijzerT,
                ijzerHaem: product.ijzerHaem,
                ijzerNonHaem: product.ijzerNonHaem,
                natrium: product.natrium,
                kalium: product.kalium,
                magnesium: product.magnesium,
                zink: product.zink,
                selenium: product.selenium,
                koper: product.koper,
                jodium: product.jodium,
                retinol: product.retinol,
                bCarteen: product.bCarteen,
                vitB1: product.vitB1,
                vitB2: product.vitB2,
                vitB6: product.vitB6,
                vitB12: product.vitB12,
                vitD: product.vitD,
                vitE: product.vitE,
                vitC: product.vitC,
                folaat: product.folaat,
                foliumzuur: product.foliumzuur,
                nicZuur: product.nicZuur,
                aTocoferol: product.aTocoferol,
                bTocoferol: product.bTocoferol,
                gammaTocoferol: product.gammaTocoferol,
                dTocoferol: product.dTocoferol,
                aCaroteen: product.aCaroteen,
                luteine: product.luteine,
                zeaxanthine: product.zeaxanthine,
                bCryptoxanthine: product.bCryptoxanthine,
                lycopeen: product.lycopeen,
                vitK: product.vitK,
                vitK1: product.vitK1,
                vitK2: product.vitK2,
                cholecalciferol: product.cholecalciferol,
                addedByCoach: true,
                checked: false,
                userId: 1
            };

        console.log(product);
        this.api.put<void>('products/edit', data).subscribe
        (
            data => {
                alert('Het aanpassen is gelukt');
            },
            error =>{
                alert('Het aanpassenis niet gelukt');
            }
        );
        console.log(data);
    }

}
