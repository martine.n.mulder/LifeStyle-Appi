import { Component, OnInit } from '@angular/core';
import {User} from "../user/user";
import {UserService} from "../user/user.service";
import {ListDataSource} from "./list.datasource";
import {Observable} from "rxjs/Observable";
import {getTemplate, getTemplateUrl} from "codelyzer/util/ngQuery";
import {ProductService} from "../products/product.service";
import {Product} from "../products/Product";
import {ClientService} from "../shared/client/client.service";
import {Client} from "../shared/client/client";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

    user: User = new User();
    public subscription;
    selectedUser: User;

    public addedProducts: Observable<Product[]>;
    public allClients: Observable<Client[]>;
    public allUsers: Observable<User[]>;
    public countRivm: String;

    constructor(private userService: UserService, private productService:ProductService, private clientService:ClientService) {
    }

    register(){
      this.userService.register(this.user);
    }

    updateUser(){
        this.userService.updateUser(this.user);
    }

    deleteUser(){
        this.userService.deleteUser(this.selectedUser.username)
    }

    getAll()    {
       // this.allClients = this.clientService.getAll();
        this.productService.getRivmCount().subscribe(data => {
            this.handleData(data)
        });
        this.addedProducts = this.productService.getAddedProducts();
        this.allUsers = this.userService.getAll();
    }

    private handleData(data: string) {
        console.log(data);
        this.countRivm= data;
    }

    public hasData()
    {
        return this.allUsers !== null;
    }

    public setSelectedUser(user: User){
        this.userService.setEditUser(user);
        this.userService.setSelectedUser(user);
    }

    ngOnInit() {
        //this.getAll();
        this.userService.refreshUserList();
        this.subscription = this.userService.selectedUser.subscribe(
            selectedUser => {this.selectedUser = selectedUser;}
        );
    }
}
