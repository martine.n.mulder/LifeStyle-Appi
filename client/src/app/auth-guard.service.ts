import { Injectable }     from '@angular/core';
import {CanActivate, Router} from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

    private role;
    protected debug = false //if debug is true you are allowed on all the pages. without credentials
    constructor(private router: Router) {
        this.debug = false;
    }


    canActivate() {
        if (!this.debug) {
            return this.isLoggedin();
        }else{return this.debug}
    }

    public isLoggedin(): boolean {

        let authorizationString = sessionStorage.getItem('authorization');
        let firstName = sessionStorage.getItem('firstName');
        let role = sessionStorage.getItem('role');

        if (authorizationString === null) {
            console.log("not session use localStorage")
            authorizationString = localStorage.getItem('authorization');
            firstName = localStorage.getItem('firstName');
            role = localStorage.getItem('role');
        }
        if (authorizationString === null ||firstName === null || role === null ){
            console.log("acces denied - USER NOT LOGGED-IN")
            this.router.navigate(['login']);
            return false

        }else{
            this.role = role;
            return true
        }
    }

    public getRole() :string{
        return this.role;
    }
    public getRouter() :Router{
        return this.router;
}





}
