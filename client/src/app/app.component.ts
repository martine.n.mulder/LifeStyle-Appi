import { Component } from '@angular/core';
import { ClientService } from './shared/client/client.service';
import { AuthorizationService } from "./shared/authorization.service";
import { ApiService } from "./shared/api.service";
import { HttpHandler } from '@angular/common/http';
import { Router } from "@angular/router";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers: [
        ClientService,
        ApiService,
        AuthorizationService
    ]
})

export class AppComponent {

    constructor(public router: Router){}
}
