import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IntakeComponent } from './intake/intake.component';
import { ProductsComponent } from './products/products.component';
import { LoginComponent } from './user/login/login.component';
import { AdminComponent } from "./admin/admin.component";
import {UserComponent} from "./user/user.component";
import {AddProductComponent} from "./products/add-product/add-product.component";
import {AuthGuard} from "./auth-guard.service";
import {AuthAdminGuard} from "./auth-admin-guard.service";
import {DidyouknowComponent} from "./didyouknow/didyouknow.component";
import {AddDidYouKnowComponent} from "./didyouknow/add-did-you-know/add-did-you-know.component";

const routes: Routes = [
    { path: '', component: IntakeComponent, canActivate:[AuthGuard] },
    { path: 'producten', component: ProductsComponent , canActivate:[AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'admin', component: AdminComponent, canActivate:[AuthAdminGuard]},
    { path: 'gebruiker', component: UserComponent, canActivate:[AuthGuard]},
    { path: 'producten/add', component: AddProductComponent},
    { path: 'wistjedat', component: DidyouknowComponent},
    { path: 'wistjedat/add', component: AddDidYouKnowComponent}
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})

export class AppRoutingModule {

}

