import {Injectable} from "@angular/core";
import {ApiService} from "../shared/api.service";
import {AuthorizationService} from "../shared/authorization.service";
import {LoginComponent} from "../user/login/login.component";
import {DidYouKnow} from "./DidYouKnow";


@Injectable()
export class DidyouknowService{

    constructor(private api: ApiService, private authService: AuthorizationService, private loginComponent: LoginComponent){}

    public getAllDidyoukows(didyouknow: DidYouKnow):void {
        let data =
            {
                id:didyouknow.id,
                userId: didyouknow.userId,
                string: didyouknow.string,
                title: didyouknow.title,
                category: didyouknow.category,
                checked: didyouknow.checked
            }
    }

    public uploadFile(FormData):void {
        this.api.post('didYouKnow/upload', FormData).subscribe(
            data => {},
            (err:any) => {});
    }

    public addDidYouKnow(didYouKnow : DidYouKnow):void{
        let data = {
            userId: 1,
            body: didYouKnow.string,
            title: didYouKnow.title,
            category: didYouKnow.category,
            checked: false
        };

        this.api.post<void>('didYouKnow/add', data).subscribe(
            data => {
                alert('Het toevoegen is gelukt');
            },
            error =>{
                alert('Het toevoegen is niet gelukt');
            }
        )
    }

    public updateDidYouKnow(didYouKnow : DidYouKnow):void{
        let data = {
            id: didYouKnow.id,
            userId: didYouKnow.userId,
            body: didYouKnow.string,
            title: didYouKnow.title,
            category: didYouKnow.category,
            checked: false
        };

        this.api.post<void>('didYouKnow/update', data).subscribe(
            data => {
                alert('Het aanpassen is gelukt');
            },
            error =>{
                alert('Het aanpassen is niet gelukt');
            }
        )
    }

    public acceptDidYouKnow(didYouKnow : DidYouKnow):void{
        let data = {
            id: didYouKnow.id,
            userId: didYouKnow.userId,
            body: didYouKnow.string,
            title: didYouKnow.title,
            category: didYouKnow.category,
            checked: didYouKnow.checked
        };

        this.api.post('didYouKnow/isChecked', data).subscribe(
            data => {
                alert('Het wist-je-datje goedkeuren is gelukt');
            },
            error =>{
                alert('Het wist-je-datje goedkeuren is mislukt');
            }
        )
    }

    public deleteDidYouKnow(didYouKnow: DidYouKnow):void{
        this.api.delete('didYouKnow/delete', {id: didYouKnow.id}).subscribe(
            data => {
                alert('Het verwijderen is gelukt');
            },
            error =>{
                alert('Het verwijderen is niet gelukt');
            }
        )
    }
}
