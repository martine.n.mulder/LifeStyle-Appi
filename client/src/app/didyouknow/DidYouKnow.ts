/**
 * Created by Luc Dijkstra on 18-Dec-17.
 */

export class DidYouKnow {
    constructor(
       public id?: number,
       public userId?: number,
       public string?: string,
       public title?: string,
       public category?: string,
       public checked?: boolean
    ){}
}
