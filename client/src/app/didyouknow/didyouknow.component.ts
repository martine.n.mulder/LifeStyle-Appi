import {Component, OnInit, ViewChild} from '@angular/core';
import {DidyouknowService} from "./didyouknow.service";
import {DidYouKnow} from "./DidYouKnow";

@Component({
  selector: 'app-didyouknow',
  templateUrl: './didyouknow.component.html',
  styleUrls: ['./didyouknow.component.css'],
  providers: [DidyouknowService],
})
export class DidyouknowComponent implements OnInit {
    @ViewChild("files") fileInput;
    changeDidYouKnow: DidYouKnow = new DidYouKnow();
    acceptedDidYouKnow: DidYouKnow = new DidYouKnow();
    deletedDidYouKnow: DidYouKnow = new DidYouKnow();

  constructor(private didyouknowService: DidyouknowService) { }

  ngOnInit() {
  }

  uploadFile(): void {
      let fileBrowser = this.fileInput.nativeElement;
      if(fileBrowser.files && fileBrowser.files[0]){
          const formData = new FormData();
          formData.append("file", fileBrowser.files[0]);
          console.log("Filebroswer: " + fileBrowser.files[0].name);
          this.didyouknowService.uploadFile(formData);
      }
  }

  acceptDidYouKnow(): void {
      this.didyouknowService.acceptDidYouKnow(this.acceptedDidYouKnow);
  }

  editDidYouKnow(): void {
      this.didyouknowService.updateDidYouKnow(this.changeDidYouKnow);
  }

  deleteDidYouKnow(): void {
      this.didyouknowService.deleteDidYouKnow(this.deletedDidYouKnow);
  }

}
