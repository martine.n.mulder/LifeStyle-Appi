import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDidYouKnowComponent } from './add-did-you-know.component';

describe('AddDidYouKnowComponent', () => {
  let component: AddDidYouKnowComponent;
  let fixture: ComponentFixture<AddDidYouKnowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDidYouKnowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDidYouKnowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
