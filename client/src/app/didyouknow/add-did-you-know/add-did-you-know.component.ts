import { Component, OnInit } from '@angular/core';
import {DidyouknowComponent} from "../didyouknow.component";
import {DidyouknowService} from "../didyouknow.service";
import {DidYouKnow} from "../DidYouKnow";

@Component({
  selector: 'app-add-did-you-know',
  templateUrl: './add-did-you-know.component.html',
  styleUrls: ['./add-did-you-know.component.css'],
  providers: [DidyouknowService]
})
export class AddDidYouKnowComponent implements OnInit {
    DidYouKnow: DidYouKnow = new DidYouKnow();

  constructor(private didYouKnowService: DidyouknowService) { }

  ngOnInit() {
  }

  addDidYouKnow(){
      this.didYouKnowService.addDidYouKnow(this.DidYouKnow)
  }

}
