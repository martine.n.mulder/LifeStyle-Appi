import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthorizationService } from './authorization.service';
import { ApiService } from './api.service';
import { ClientService } from "./client/client.service";
import {ProductService} from "../products/product.service";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
    providers: [
        ApiService,
        AuthorizationService,
        ClientService,
        ProductService

    ],
})

export class SharedModule {

}
