import {Component, OnInit} from '@angular/core';
import {ClientFormService} from "./client-form/client-form.service";
import {ClientService} from "../client/client.service";
import {Client} from "../client/client";

@Component({
    selector: 'app-right-menu',
    templateUrl: './right-menu.component.html',
    styleUrls: ['./right-menu.component.css']
})

export class RightMenuComponent implements OnInit {

    selectedClient: Client;

    constructor(private clientFormService: ClientFormService, public clientService: ClientService) {
    }

    ngOnInit() {
        // this.clientService.selectedClient.subscribe(selectedClient => {
        //     this.selectedClient = selectedClient;
        // });
        this.selectedClient = this.clientService.selectedClient;
    }

    openForm(action) {
        this.clientFormService.setFormAction(action);
    }

}
