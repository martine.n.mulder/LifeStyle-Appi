import {Injectable} from '@angular/core';

@Injectable()
export class ClientFormService {

    showForm: boolean = false;
    formEdit: boolean = false;
    formAdd: boolean = false;

    constructor() {

    }

    setFormAction(action: string) {
        this.showForm = true;
        if (action == 'add') {
            this.formAdd = true;
        } else {
            this.formEdit = true;
        }
    }

    closeForm() {
        this.showForm = false;
        this.formEdit = false;
        this.formAdd = false;
    }

}
