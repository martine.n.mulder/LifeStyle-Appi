import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../client/client.service';
import { Client } from "../../client/client";
import { ClientFormService } from "./client-form.service";

@Component({
    selector: 'app-client-form',
    templateUrl: './client-form.component.html',
    styleUrls: ['./client-form.component.css'],
})

export class ClientFormComponent implements OnInit {

    client: Client = new Client();
    selectedClient: Client;
    submitted: boolean = false;

    constructor(public clientService: ClientService, public clientFormService: ClientFormService) {
    }

    ngOnInit() {
        // this.selectedClient = this.clientService.selectedClient;
        this.clientService.selectedClient.subscribe(selectedClient => {
            this.selectedClient = selectedClient;
        });
        // this.clientService.selectedClient.subscribe(console.log);
    }

    createClient() {

        this.submitted = true;

        if (this.client.firstName == null || this.client.lastName == null || this.client.birthDate == null
            || this.client.weight == 0 || this.client.weight == null || this.client.gender == null) {
        } else {
            this.clientService.create(this.client);
            this.clientFormService.showForm = false;
            this.clientFormService.formEdit = false;
            this.clientFormService.formAdd = false;
        }
    }

    updateClient() {
        this.submitted = true;

        if (this.selectedClient == null || this.selectedClient.lastName == null || this.selectedClient.birthDate == null
            || this.selectedClient.weight == 0 || this.selectedClient.weight == null || this.selectedClient.gender == null) {
        } else {
            this.clientService.updateClient(this.selectedClient);
            this.clientFormService.showForm = false;
            this.clientFormService.formEdit = false;
            this.clientFormService.formAdd = false;
        }
    }

    closeForm() {
        this.clientFormService.closeForm();
    }

}
