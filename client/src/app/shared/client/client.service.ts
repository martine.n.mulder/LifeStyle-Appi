import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ApiService } from '../api.service';
import { Client } from './client';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import { Subject } from "rxjs/Subject";

@Injectable()
export class ClientService {

    clients$: Observable<Client[]>;
    client: Client;
    selectedClient: Subject<Client> = new Subject<Client>();
    selectedDate: Date;

    constructor(private api: ApiService) {

    }

    public getAll(): Observable<Client[]> {
        return this.api.get<Client[]>('clients');
    }

    public refreshList() {
        this.clients$ = this.getAll();
    }

    public setSelectedClient(client: Client) {
        if (client) {
            this.selectedClient.next(client);
        }
        let clientString = JSON.stringify(client);
        sessionStorage.setItem('currentClient',clientString);
    }

    public getSelectedClient() {
        let client = JSON.parse(sessionStorage.getItem('currentClient'));

        if (client) {
            this.selectedClient.next(client);
        }
        return this.selectedClient.asObservable();
    }

    public getSelectedDate() {
        if (sessionStorage.getItem('currentDate')) {
            this.selectedDate = new Date(Date.parse(JSON.parse(sessionStorage.getItem('currentDate'))));
        } else {
            this.selectedDate = new Date(Date.now());
        }
        return this.selectedDate;
    }

    public setSelectedDate(selectedDate: Date) {
        this.selectedDate = selectedDate;
        let dateString = JSON.stringify(selectedDate);
        sessionStorage.setItem('currentDate',dateString);
    }

    public create(client: Client): void {
        let data = {
                firstName: client.firstName,
                preposition: client.preposition,
                lastName: client.lastName,
                active: client.active,
                weight: client.weight,
                birthDate: client.birthDate,
                gender: client.gender
            };
        this.api.post<void>('clients', data).subscribe
        (
            data => {
                alert('Cliënt aanmaken is gelukt');
                this.clients$ = this.getAll();
            },
            error =>{
                alert('Cliënt aanmaken is niet gelukt');
            }
        )
    }

    public updateClient(client: Client): void {
        let data = {
            id: client.id,
            firstName: client.firstName,
            preposition: client.preposition,
            lastName: client.lastName,
            active: client.active,
            weight: client.weight,
            birthDate: client.birthDate,
            gender: client.gender
        };

        this.api.put<void>('clients', data).subscribe
        (
            data => {
                console.log('Het updaten is gelukt');
            },
            error =>{
                console.log('Het updaten is niet gelukt');
            }
        )
    }

}
