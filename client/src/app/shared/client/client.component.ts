import { Component, OnInit } from '@angular/core';
import { Client } from "./client";
import { ClientService } from "./client.service";
import { ApiService } from "../api.service";
import { AuthorizationService } from "../authorization.service";

@Component({
    selector: 'app-client',
    templateUrl: './client.component.html',
    styleUrls: ['./client.component.css'],
    providers: [ ClientService ]
})

export class ClientComponent implements OnInit {


    constructor() {
    }

    ngOnInit() {
    }

}
