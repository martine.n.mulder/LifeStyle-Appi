export class Client {

    constructor(
        public id?: number,
        public firstName?: string,
        public preposition?: string,
        public lastName?: string,
        public coachId?: number,
        public active?: number,
        public weight?: number,
        public birthDate?: string,
        public gender?: string,
        public name?: string,
        )
    {
    }

}
