import { Component, OnInit, Input } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { ClientService } from "../client/client.service";
import { Client } from "../client/client";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import { UserService } from "../../user/user.service";
import { IntakeService } from "../../intake/intake.service";
import {Router} from "@angular/router";
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatDatepicker, MatDialog} from "@angular/material";

@Component({
    selector: 'app-left-menu',
    templateUrl: './left-menu.component.html',
    styleUrls: ['./left-menu.component.css']
})

export class LeftMenuComponent implements OnInit {

    selectedClient: Client;
    firstName : string;
    selectedDate = new Date(Date.now());
    clients: Client[];

    constructor(private adapter: DateAdapter<any>, public router: Router, public clientService: ClientService, public userService: UserService, public intakeService: IntakeService) {
        this.adapter.setLocale('nl');
    }

    ngOnInit() {

        this.getSelectedClient();
        this.getSelectedDate();

        let userFirstName = sessionStorage.getItem('firstName');

        // Check if session = null if so use localstorage
        if (userFirstName === null) {
            userFirstName = localStorage.getItem('firstName');
        }
        this.firstName = userFirstName;

        // If firstname is not null, means that the user is logged-in and the client list can be retrieved.
        if (this.firstName !== null) {
            this.clientService.refreshList();
        }

    }

    public getSelectedClient() {
        this.clientService.getSelectedClient().subscribe(selectedClient => {
            this.selectedClient = selectedClient;
        });
    }

    public setSelectedClient(client: Client) {
        this.clientService.setSelectedClient(client);
        this.intakeService.setSelectedClient(client);
    }

    public getSelectedDate() {
        this.selectedDate = this.clientService.getSelectedDate();
    }

    public setSelectedDate() {
        this.clientService.setSelectedDate(this.selectedDate);
        this.clientService.setSelectedClient(this.selectedClient);
    }

    // Clears all locals and sessions.
    public logout(){
        sessionStorage.clear();
        localStorage.clear();
    }


}
