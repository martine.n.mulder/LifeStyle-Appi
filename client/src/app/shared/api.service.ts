import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthorizationService} from "./authorization.service";
import {Product} from "../products/Product";
import { RequestOptions } from '@angular/http';

@Injectable()
export class ApiService {

    constructor(private http: HttpClient, private authService: AuthorizationService) {
    }

    private createQueryString(queryParameters: Object): string {
        let queryString = '';
        if (typeof queryParameters === 'string') {
            queryString = '/${queryParameters}'
        }

        if (typeof queryParameters === 'object') {
            for (let key in queryParameters) {
                let value = queryParameters[key];
                let prefix = queryString.length === 0 ? '?' : '&';

                queryString += `${prefix}${key}=${value}`;
            }
        }

        return queryString;
    }
/*
    private createRequestHeaders(): HttpHeaders {
        let headers = new HttpHeaders();
       console.log(headers.has('Authorization'));

        if (this.authService.hasAuthorization()) {
            var authKey = this.authService.createAuthorizationString()

            headers = headers.set('Authorization', 'ERROR!');
            console.log("SETTING HEADER AUTHORIZATION")
            console.log(authKey)
            headers = headers.set('Authorization', authKey);
            console.log("SETTED!")

        }
        headers = headers.set('test1', 'asdasdsadasd');
        headers = headers.set('test2', 'Baasdasdsadsad4==');
        headers = headers.set('test3', 'Basdasdasd');
        //headers = headers.set('Authorization', authKey);
        console.log(headers.has('Authorization'));
        return headers;
    }*/
/*
        private createRequestHeaders(): HttpHeaders {

            let headers = new HttpHeaders();
            console.log("Creating headers.")

            if (this.authService.hasAuthorization()) {
                console.log(this.authService.hasAuthorization());
                console.log(this.authService.getUserName())
                console.log()
                console.log("creating Header with auth")
                var authKey = this.authService.createAuthorizationString()
                headers.delete('Authorization');
                headers = headers.set('Authorization', authKey);
                headers = headers.set('test2', this.authService.getUserName() + "test");
                headers.delete('Authorization');
            }else {
                console.log('no auth has been found.')
                headers = headers.set('test2', this.authService.getUserName() + "test");
                headers = headers.set('Authorization', 'Basic invalid==');
            }
            return headers;
        }*/
    private createRequestHeaders(): HttpHeaders {
        let headers = new HttpHeaders();
        this.authService.restoreAuthorization();
        var authKey = this.authService.createAuthorizationString()
        headers = headers.set('Authorization', authKey);
        return headers;
    }

    private createURI(path: string, queryParameters: Object): string {
        let queryString = this.createQueryString(queryParameters);
        return `/api/${path}${queryString}`;
    }

    public get <T>(path: string, queryParameters?: Object): Observable<T> {
        let uri = this.createURI(path, queryParameters);
        let headers = this.createRequestHeaders();
        return this.http.get<T>(uri, { headers: headers });
    }

    public post<T>(path: string, data: Object, queryParameters?: Object): Observable<T> {
        let uri = this.createURI(path, queryParameters);
        let headers = this.createRequestHeaders();

        return this.http.post<T>(uri, data, {headers: headers});
    }

    public put<T>(path: string, data: Object, queryParameters?: Object): Observable<T> {
        let uri = this.createURI(path, queryParameters);
        let headers = this.createRequestHeaders();
        return this.http.put(uri, data, {headers: headers});
    }

    public delete <T>(path: string, queryParameters?: Object): Observable<T>  {
        let uri = this.createURI(path, queryParameters);
        let headers = this.createRequestHeaders();

        return this.http.delete(uri, {headers: headers});
    }
}
