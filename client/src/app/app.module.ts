import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms'; // <-- NgModel lives here
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { LeftMenuComponent } from './shared/left-menu/left-menu.component';
import { RightMenuComponent } from './shared/right-menu/right-menu.component';
import { MessageComponent } from './message/message.component';
import { MessageService } from './message/message.service';
import { AppRoutingModule } from './/app-routing.module';
import { IntakeComponent } from './intake/intake.component';
import { ProductsComponent } from './products/products.component';
import { LoginComponent } from './user/login/login.component';
import { UserComponent } from './user/user.component';
import { ClientFormComponent } from './shared/right-menu/client-form/client-form.component';
import { UserService } from "./user/user.service";
import { ApiService } from "./shared/api.service";
import { AuthorizationService } from "./shared/authorization.service";
import { HttpClientModule } from "@angular/common/http";
import { AdminComponent } from './admin/admin.component';
import { ClientComponent } from './shared/client/client.component';
import { ClientService } from "./shared/client/client.service";
import { HttpModule } from "@angular/http";
import { ClientFormService } from "./shared/right-menu/client-form/client-form.service";
import { AddProductComponent } from './products/add-product/add-product.component';
import {ProductService} from "./products/product.service";
import {AuthGuard} from "./auth-guard.service";
import {AuthAdminGuard} from "./auth-admin-guard.service";
import { DidyouknowComponent } from './didyouknow/didyouknow.component';
import {MatListModule} from '@angular/material/list';
import { AddDidYouKnowComponent } from './didyouknow/add-did-you-know/add-did-you-know.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
    MatAutocompleteModule, MatFormFieldModule, MatInputModule, MatTableModule,
    MatTabsModule, MatSortModule, MatPaginatorModule, MatDatepicker, MatDatepickerContent, MatDatepickerInput,
    MatDatepickerToggle, MatMonthView, MatYearView, MatCalendar, MatCalendarBody, DateAdapter, MatNativeDateModule,
    MatDialogModule, MAT_DATEPICKER_SCROLL_STRATEGY_PROVIDER, MatDatepickerIntl, MAT_DATE_LOCALE, MatSelectModule,
    MatDialog
} from "@angular/material";
import { IntakeService } from "./intake/intake.service";


@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        LeftMenuComponent,
        RightMenuComponent,
        MessageComponent,
        IntakeComponent,
        ProductsComponent,
        LoginComponent,
        UserComponent,
        ClientFormComponent,
        AdminComponent,
        ClientComponent,
        AddProductComponent,
        LoginComponent,
        DidyouknowComponent,
        AddDidYouKnowComponent,
        MatCalendar,
        MatCalendarBody,
        MatDatepicker,
        MatDatepickerContent,
        MatDatepickerInput,
        MatDatepickerToggle,
        MatMonthView,
        MatYearView
    ],
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutingModule,
        HttpClientModule,
        Ng2SmartTableModule,
        HttpModule,
        MatListModule,
        MatNativeDateModule,
        MatAutocompleteModule,
        MatTableModule,
        MatTabsModule,
        MatSortModule,
        MatPaginatorModule,
        MatDialogModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatSelectModule
    ],
    providers: [
        MessageService,
        UserService,
        ApiService,
        AuthorizationService,
        ClientService,
        ClientFormService,
        ProductService,
        LoginComponent,
        ClientFormService,
        AuthGuard,
        AuthAdminGuard,
        IntakeService,
        MatDatepickerIntl,
        MAT_DATEPICKER_SCROLL_STRATEGY_PROVIDER
    ],
    entryComponents: [
        MatDatepickerContent
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
