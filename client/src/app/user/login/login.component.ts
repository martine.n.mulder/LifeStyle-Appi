import { Component, OnInit } from '@angular/core';

import { User} from "../user";
import { UserService } from '../user.service';
import {Router} from "@angular/router";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    user: User = new User();
    remember;

    constructor(private userService: UserService, private router: Router) {
        console.log('login page construct')
    }

    ngOnInit() {
        let authorizationString = sessionStorage.getItem('authorization');

        if (authorizationString === null) {
            authorizationString = localStorage.getItem('authorization');
        }
        if(authorizationString !== null){
            this.router.navigate([''])
        }
    }

    onSubmit() {
        this.userService.login(this.user, this.remember);
    }


}
