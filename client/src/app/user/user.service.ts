import { Injectable } from '@angular/core';
import {User} from "./user";
import { AuthorizationService } from '../shared/authorization.service';
import {ApiService} from "../shared/api.service";
import {Router} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {Subject} from "rxjs/Subject";

@Injectable()
export class UserService {
    selectedUser: Subject<User> = new Subject<User>();
    allUsers$: Observable<User>;
    editUser: User;

  constructor(private api: ApiService, private authService: AuthorizationService,private router: Router) {

  }

    public login(user: User, remember: boolean): void
    {
      this.authService.setAuthorization(user.username, user.password);
        this.api.get<User>('users/login').subscribe(
            //on succes
            authenticator =>
            {
                this.authService.storeAuthorization(authenticator, remember);
                this.setLoginSession(authenticator, remember);

                this.goHome();

            },
            //on fail
            error =>
            {
                alert('Het inloggen is mislukt');
            }
        )
    }

    public setLoginSession(User: User, local: boolean): void{
        let storage = local ? localStorage : sessionStorage;

        storage.setItem('firstName', User.firstname);
        storage.setItem('role', User.role);
    }


    public register(user: User): void
    {
        let data =
            {
                username: user.username,
                password: user.password,
                firstname: user.firstname,
                preposition: user.preposition,
                lastname: user.lastname,
                email: user.email,
                role: user.role
            };
        this.api.post<void>('users', data).subscribe
        (
            data => {
                alert('Het aanmaken is gelukt');
            },
            error =>{
                    alert('Het aanmaken is niet gelukt');
            }
        )
    }

    public updateUser(user: User): void
    {
        if(user.firstname == null || user.firstname == ''){
            user.firstname = this.editUser.firstname;
        }
        if(user.preposition == null){
            user.preposition = this.editUser.preposition;
        }
        if(user.lastname == null){
            user.lastname = this.editUser.lastname;
        }
        if(user.username == null){
            user.username = this.editUser.username;
        }
        if(user.password == null){
            user.password = this.editUser.password;
        }
        if(user.email == null){
            user.email = this.editUser.email;
        }
        if(user.role == null){
            user.role = this.editUser.role;
        }

        let data =
            {
                firstname: user.firstname,
                preposition: user.preposition,
                lastname: user.lastname,
                username: user.username,
                password: user.password,
                email: user.email,
                role: user.role
            };

        this.api.post<void>('users/update', data).subscribe
        (
            data => {
                alert('Het aanmaken is gelukt');
            },
            error =>{
                alert('Het aanmaken is niet gelukt');
            }
        )
    }

    public deleteUser(username: string): void {
        console.log(username);
        this.api.delete('users/delete/' + username).subscribe();
    }

    public getAll(): Observable<User[]> {
      console.log('get users')
        return this.api.get<User[]>('users');
    }

    public setSelectedUser(user: User){
        if(user){
            this.selectedUser.next(user);
        }
    }

    public setEditUser(user: User){
        this.editUser = user;
    }

    private goHome()
    {
        this.router.navigate(['']);
    }


   public refreshUserList() {
        this.allUsers$ = this.getAll();
    }
}
