///<reference path="../../../node_modules/@angular/cdk/typings/rxjs/rx-operators.d.ts"/>
import {AfterViewInit, Component, OnInit} from '@angular/core';
import { ClientService } from "../shared/client/client.service";
import { Client } from "../shared/client/client";
import { Subscription } from "rxjs/Subscription";
import { IntakeService } from "./intake.service";
import { Observable } from "rxjs/Observable";
import { Intake } from "./intake";
import { ProductService } from "../products/product.service";
import { Product } from "../products/Product";
import { FormControl } from '@angular/forms';
import {Subject} from "rxjs/Subject";
import {map} from "rxjs/operator/map";
import {startWith} from "@angular/cdk/typings/rxjs";
//import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import "rxjs/add/operator/startWith";
import "rxjs/add/operator/first";
import "rxjs/add/operator/switchMap";
import { IntakeDataSource } from "./intake.datasource";
import {UserService} from "../user/user.service";
import {count} from "rxjs/operators";

@Component({
    selector: 'app-intake',
    templateUrl: './intake.component.html',
    styleUrls: ['./intake.component.css']
})

export class IntakeComponent implements OnInit, AfterViewInit {

    newIntake: Intake = new Intake();
    loadIntake: boolean = false;
    product: Product;
    newProductId: number;
    newProduct: Product = new Product();

    products: Product[];
    selectedClient: Client;
    selectedDate: string;

    myControl: FormControl;
    intakes: Intake[];

    filteredProducts: Product[];

    kcal: boolean = true;
    kjoule: boolean = true;
    koolhydraten: boolean = true;
    alcohol: boolean = true;
    water: boolean = true;
    vezels: boolean = true;

    suiker: boolean = false;
    suikerSplit: boolean = false;

    eiwit: boolean = false;
    eiwitSplit: boolean = false;

    vet: boolean = false;
    vetSplit: boolean = false;

    mineralenSplit: boolean = false;
    vitaminesSplit: boolean = false;

    filterOpen: boolean = false;


    constructor(public userService: UserService, public clientService: ClientService, public intakeService: IntakeService, public productService: ProductService) {
        this.productService.createProductArray();
    }

    ngAfterViewInit() {
    }

    ngOnInit() {

        this.getProducts();

        // Get selected date
        this.getSelectedDate();

        // Catch event change in select client and retrieve intake if a date is also selected
        this.clientService.selectedClient.forEach((client) => {
            this.selectedClient = client;

            this.getIntake();
            this.getSelectedDate();

        });

        this.myControl = new FormControl();

        this.clientService.getSelectedClient().subscribe(selectedClient => {
            this.selectedClient = selectedClient;
        });

        this.myControl.valueChanges.subscribe(data => {
            if (data != "") {
                // data = data.toLowerCase();
                this.filteredProducts = this.productService.productArray.filter(product => product.description.toLowerCase().startsWith(data));
            }
        })
    }

    public getSelectedDate() {
        let date = this.clientService.getSelectedDate();
        this.selectedDate = date.getFullYear()+"-"+date.getMonth() + 1 +"-"+date.getDate();
    }

    public getProducts() {

        this.productService.getAllProducts().subscribe(
            products => {
                this.products = products;
            }
        );
    }

    public getIntake() {

        // setTimeout(() => {
            this.getSelectedDate();

            this.intakeService.getIntakePerDate(this.selectedClient.id, this.selectedDate).subscribe(
                intake => {
                    // this.intakes = intake;
                    this.intakes = intake.map(intake => { return {
                        intake: intake,
                        product: this.getProductById(intake.productId)};
                    });
                    console.log(this.intakes);
                }
            );

            this.loadIntake = true;
        // });
    }

    public addIntake() {
        this.newIntake.time = this.newIntake.time + ':00';
        this.newIntake.productId = this.newProduct.id;
        this.newIntake.date = this.selectedDate;
        this.newIntake.clientId = this.selectedClient.id;
        this.newIntake.meal = "ontbijt";
        console.log(this.newIntake);

        this.intakeService.addIntake(this.newIntake);
        this.newIntake = new Intake();
        this.newProduct = new Product();
        setTimeout(() => {
            this.getIntake();
        },500);
    }

    public setNewProduct(product: Product) {
        this.newProduct = product;
    }

    public editIntake(intake: Intake, productId: number, refresh: boolean) {
        intake.time = intake.time + ':00';
        intake.productId = productId;
        this.intakeService.editIntake(intake);
        if (refresh) {
            this.getIntake();
        }
    }

    public delete(intake: Intake) {
        this.intakeService.deleteIntake(intake);
        this.getIntake();
    }

    public getProduct(id: number) {
        // setTimeout(function() {
        //     return this.products.find(product => product.id === id);
        //
        // },500);
        // console.log('Getting product with id ' + id);
        // // let product: Product = new Product;
        // console.log(id);
        //
        if (this.products) {
            console.log(this.products.find(product => product.id === id).description);

            return this.products.find(product => product.id === id);

        } else {
            return null;
        }
    }

    public getProductById(id: number) {

        let foundProduct: Product = new Product();

        this.productService.getProductById(id).subscribe(
            product => {
                foundProduct = product;
            }
        );
        return this.productService.getProductById(id);
        // return foundProduct;

    }

    public returnNutrient(number: string, amount: number) {

        if (number != null) {
            let nutrient = parseInt(number);
            nutrient = (nutrient / 100 * amount);
            nutrient = Math.round(nutrient * 100) / 100
            let nutrientString = nutrient.toString();
            return nutrientString;
        } else {
            return "";
        }
        // if (number == "") {
        //     return 0;
        // } else {
        //     return parseFloat(number);
        // }

    }

}
