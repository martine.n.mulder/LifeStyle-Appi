import {Product} from "../products/Product";

export class Intake {

    constructor(
        public id?: number,
        public clientId?: number,
        public time?: string,
        public productId?: number,
        public amount?: number,
        public meal?: string,
        public date?: string,
        public product?: Product
    )
    {
    }

}
