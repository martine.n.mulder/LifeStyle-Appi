import {Injectable} from '@angular/core';
import {ApiService} from "../shared/api.service";
import {Observable} from "rxjs/Observable";
import {Intake} from "./intake";
import {ClientService} from "../shared/client/client.service";
import {Client} from "../shared/client/client";
import {Http} from "@angular/http";
import {Subject} from "rxjs/Subject";
import {BehaviorSubject} from "rxjs/BehaviorSubject";

@Injectable()
export class IntakeService {

    selectedClient: Client;
    public intake: Intake[] = new Array;

    constructor(private api: ApiService) {

    }

    public getIntakePerDate(currentClientId: number, currentDate: string) {
        return this.api.get<Intake[]>('intake/'+currentClientId+'/'+currentDate);
    }

    public deleteIntake(intake: Intake) {
        this.api.delete<void>('intake/'+intake.id)
            .subscribe(
                (response) => console.log('Intake successfully deleted.'),
                (error) => console.log('Intake was not removed')
            );
    }

    public setSelectedClient(client: Client) {
        this.selectedClient = client;
    }

    flatten(obj) {
        return Object.keys(obj).reduce(function(previous, current) {
            previous.push(obj[current]);
            return previous;
        }, []);
    }

    public editIntake(intake: Intake): void {

        let data = {
            id: intake.id,
            clientId: intake.clientId,
            time: intake.time,
            productId: intake.productId,
            amount: intake.amount,
            meal: intake.meal,
            date: intake.date
        };

        this.api.put<void>('intake', data).subscribe
        (
            data => {
                console.log('Het updaten is gelukt');
            },
            error =>{
                console.log('Het updaten is niet gelukt');
            }
        )
    }

    public addIntake(intake: Intake): void {

        let data = {
            id: intake.id,
            clientId: intake.clientId,
            time: intake.time,
            productId: intake.productId,
            amount: intake.amount,
            meal: intake.meal,
            date: intake.date
        };

        console.log(data);

        this.api.post<void>('intake', data).subscribe
        (
            data => {
                console.log('Het aanmaken is gelukt');
            },
            error =>{
                console.log('Het aanmaken is niet gelukt');
            }
        )
    }

}
