import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { Intake } from "./intake";

export class IntakeDataSource extends DataSource<any> {

    constructor(private intake: Intake[]) {
        super();
    }

    public connect(): Observable<Intake[]> {
        return Observable.of(this.intake);
    }

    public disconnect() {}

}
